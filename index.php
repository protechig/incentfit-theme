<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package IncentFit
 */

get_header();
?>

<div id="primary" class="content-area bg-white">
	<main id="main" class="site-main p-0">
		<!-- <div class="container-fluid p-0"> -->
		<?php if ( have_posts() ) : ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>

			<!-- HERO START -->
			<div class="page-header header-filter header-small bg-info" data-parallax="true" style="background: url('<?php echo $image['0']; ?>'); background-repeat:  no-repeat; background-size: cover; background-position: center center;">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 ml-auto mr-auto text-center">
						<?php
							if ( is_home() ) {
							?>
								<h1 class="page-title title">Wellness Word</h1>
							<?php
							} else {
							the_archive_title( '<h1 class="page-title title">', '</h1>' );
							the_archive_description( '<div class="archive-description">', '</div>' );
							}
						?>
						</div><!-- .col -->
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- .page-header -->
			<!-- HERO END -->
		<!-- MAIN CONTENT START -->
		<div class="main main-raised mb-5 blogs-3">
			<div class="container">
					<div class="row justify-content-center p-3">
						<div class="col col-lg-8 col-sm-12">
							<?php
								/* Start the Loop */
								while ( have_posts() ) :
								the_post();
								/*
								* Include the Post-Type-specific template for the content.
								* If you want to override this in a child theme, then include a file
								* called content-___.php (where ___ is the Post Type name) and that will be used instead.
								*/
								get_template_part( 'template-parts/content', get_post_type() );

								endwhile;

								$pagination = get_the_posts_pagination(
									 array(
										 'mid_size' => 10,
										 'screen_reader_text' => '&nbsp;',

									 )
									);

								the_posts_pagination( $pagination );
								?>
								<form role="search" method="get" class="search-form border-top mt-5 pt-5" action="<?php echo home_url( '/' ); ?>">
								  <div class="input-group">
									<input type="text" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Search Blog Posts', 'placeholder' ); ?>" value="<?php echo get_search_query(); ?>" name="s" aria-describedby="search-form">
									  <span class="input-group-btn">
										<button type="submit" class="btn btn-primary" id="search-form"><?php echo esc_attr_x( 'Search', 'submit button' ); ?>
										</button>
									  </span>
								  </div>   
								</form>
								<?php

								else :

								get_template_part( 'template-parts/content', 'none' );

							endif;
							?>
							</div><!-- .col -->
						</div><!-- .row -->
				</div><!-- container -->
			</div><!-- .main .main-raised -->
		<!-- </div>.container-fluid -->
	</main><!-- #main -->
	<!-- MAIN CONTENT END -->
</div><!-- #primary -->

<?php
get_footer();

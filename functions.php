<?php
/**
 * IncentFit functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package IncentFit
 */

if ( ! function_exists( 'incentfit_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function incentfit_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on IncentFit, use a find and replace
		 * to change 'incentfit' to the name of your theme in all the template files.
		 */
		load_theme_textdomain(
			'incentfit',
			get_template_directory() . '/languages'
		);

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'incentfit' ),
				'menu-2' => esc_html__( 'Footer', 'incentfit' ),
			)
			);

		function your_themes_pagination() {
			global $wp_query;
			echo paginate_links();
		}
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
			);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'incentfit_custom_background_args', array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
				)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo', array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
			);
	}
endif;
add_action( 'after_setup_theme', 'incentfit_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function incentfit_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'incentfit_content_width', 640 );
}
add_action( 'after_setup_theme', 'incentfit_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function incentfit_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'incentfit' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'incentfit' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
		);
}
add_action( 'widgets_init', 'incentfit_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function incentfit_scripts() {
	wp_enqueue_style( 'incentfit-style', get_stylesheet_uri() );

	wp_register_script( 'slick', get_stylesheet_directory_uri() . '/js/slick.min.js' );
	wp_enqueue_script( 'typewriter', get_stylesheet_directory_uri() . '/js/typewriter-effect.js', array(), '', false );

	wp_enqueue_script(
		'incentfit-navigation',
		get_template_directory_uri() . '/js/navigation.js',
		array(),
		'20151215',
		true
	);

	wp_enqueue_script(
		'incentfit-skip-link-focus-fix',
		get_template_directory_uri() . '/js/skip-link-focus-fix.js',
		array(),
		'20151215',
		true
	);
	wp_enqueue_script(
		'global',
		get_template_directory_uri() . '/js/global.js',
		array( 'jquery' ),
		'',
		true
	);

	wp_register_script(
		'incentfit-popper',
		get_template_directory_uri() . '/js/popper.js',
		array( 'jquery' ),
		'',
		true
	);

	wp_enqueue_script(
		'incentfit-boostrap-material',
		get_template_directory_uri() . '/js/bootstrap-material-design.min.js',
		array( 'incentfit-popper' ),
		'',
		true
	);

	wp_enqueue_script(
		'incentfit-material-kit',
		get_template_directory_uri() . '/js/material-kit.min.js',
		array( 'incentfit-boostrap-material' ),
		'',
		true
	);

	wp_register_script( 'vue', 'https://cdn.jsdelivr.net/npm/vue', array(), '', true );
	wp_register_script( 'jquery-ui-touchpunch', get_stylesheet_directory_uri() . '/js/jquery-ui-touchpunch.js', array( 'jquery-ui-core' ), '', true );
	wp_register_script( 'pricing-widget', get_stylesheet_directory_uri() . '/js/pricing-widget.js', array( 'vue', 'jquery-ui-touchpunch' ), '', true );
	 if ( is_page_template( 'products-page.php' ) ) {
	 wp_enqueue_script( 'fullpage', get_stylesheet_directory_uri() . '/js/fullpage.min.js', array(), '', true );
	 wp_enqueue_script( 'scrolloverflow', get_stylesheet_directory_uri() . '/js/scrolloverflow.min.js', array( 'fullpage' ), '', true );
	 }

	if ( is_page_template( 'page-find-a-gym.php' ) ) {
		wp_enqueue_script( 'gym-finder', get_stylesheet_directory_uri() . '/js/gym-finder.js', array( 'jquery' ) );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/**
	 * If WP is in script debug, or we pass ?script_debug in a URL - set debug to true.
	 */
	$debug =
		( defined( 'SCRIPT_DEBUG' ) && true === SCRIPT_DEBUG ) ||
		isset( $_GET['script_debug'] )
			? true
			: false;

	/**
	 * If we are debugging the site, use a unique version every page load so as to ensure no cache issues.
	 */
	$version = '1.0.0';

	/**
	 * Should we load minified files?
	 */
	$suffix = true === $debug ? '' : '.min';

	/**
	 * Global variable for IE.
	 */
	global $is_IE;

	// Register styles & scripts.
	wp_register_style(
		'incentfit-google-font',
		incentfit_font_url(),
		array(),
		null
	);

	// Enqueue styles.
	wp_enqueue_style( 'incentfit-google-font' );
	wp_enqueue_style(
		'incentfit-style',
		get_stylesheet_directory_uri() . '/style' . $suffix . '.css',
		array(),
		$version
	);

	// Enqueue scripts.
	if ( $is_IE ) {
		wp_enqueue_script(
			'incentfit-babel-polyfill',
			get_template_directory_uri() .
				'/assets/scripts/babel-polyfill.min.js',
			array(),
			$version,
			true
		);
	}

	// wp_enqueue_script( 'incentfit-scripts', get_template_directory_uri() . '/js/project.js', array( 'jquery' ), $version, true );
	wp_enqueue_script(
		'nouislider',
		get_template_directory_uri() . '/js/nouislider.min.js',
		array( 'jquery' ),
		$version,
		true
	);
	wp_enqueue_style( 'jquery-ui-slick', '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/flick/jquery-ui.min.css' );
	wp_enqueue_script(
		'jquery-ui-slider-pips',
		get_template_directory_uri() . '/js/jquery-ui-slider-pips.js',
		array( 'jquery-ui-core', 'jquery-ui-slider' ),
		$version,
		true
	);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Enqueue the scaffolding Library script.
	if ( is_page_template( 'template-scaffolding.php' ) ) {
		wp_enqueue_script(
			'incentfit-scaffolding',
			get_template_directory_uri() .
				'/assets/scripts/scaffolding' .
				$suffix .
				'.js',
			array( 'jquery' ),
			$version,
			true
		);
	}
}
add_action( 'wp_enqueue_scripts', 'incentfit_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
// require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */

require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load styles and scripts.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Load custom ACF features.
 */
require get_template_directory() . '/inc/acf.php';

/**
 * Load custom filters and hooks.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Load custom queries.
 */
require get_template_directory() . '/inc/queries.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Scaffolding Library.
 */
require get_template_directory() . '/inc/scaffolding.php';

add_image_size( 'testimonials_bio', 100, 100, true ); // cropping enabled

add_image_size( 'icon_and_text', 200, 200, true ); // cropping enabled

add_image_size( 'process', 200, 200, false ); // cropping disabled

add_image_size( 'info_cards', 400, 400, true ); // cropping disabled

add_image_size( 'brand_logos', 150, 40, false );

add_image_size( 'fifty_fifty', 640, 480, true );

add_image_size( 'fitness_challenges', 160, 280, true );

set_post_thumbnail_size( 150, 150 );

add_image_size( 'text_img_text', 260, 540, true ); // cropping disabled

add_image_size( 'bg_img', 1200, 600, false ); // cropping disabled

function cc_mime_types( $mimes ) {
 $mimes['svg'] = 'image/svg+xml';
 return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );



// rewrite rule for gym finder sites
add_action(
	'init',
	function() {
		$page = get_page_by_path( 'products/corporate-gym-discounts/find-a-gym/' );
		if ( ! empty( $page ) ) {
			add_rewrite_tag( '%location%', '([^&]+)' );
			add_rewrite_tag( '%gym%', '([^&]+)' );
			add_rewrite_rule( '^products/corporate-gym-discounts/([^/]*)/([^/]*)/?', 'index.php?page_id=' . $page->ID . '&location=$matches[1]&gym=$matches[2]', 'top' );
			add_rewrite_rule( '^products/corporate-gym-discounts/([^/]*)/?', 'index.php?page_id=' . $page->ID . '&location=$matches[1]', 'top' );
		}
	}
);

// make our Google API key available as a global variable in PHP
if ( ! defined( 'GOOGLE_API_JS_KEY' ) ) {
	define( 'GOOGLE_API_JS_KEY', 'AIzaSyBhawOKAO2nmXpo2heW5KcIA1LZrG3dlqw' );
}

/*
 Add External Sitemap to Yoast Sitemap Index
 * Credit: Paul https://wordpress.org/support/users/paulmighty/
 * Last Tested: Aug 25 2017 using Yoast SEO 5.3.2 on WordPress 4.8.1
 *********
   Format: yyyy-MM-dd'T'HH:mm:ssZ
 * Please note that changes will be applied upon next sitemap update.
 * To manually refresh the sitemap, please disable and enable the sitemaps.
 */
add_filter(
	 'wpseo_sitemap_index',
	function() {
		$sitemap_custom_items = '
<sitemap>
<loc>https://content.incentfit.com/wordpress/gyms-sitemap.xml</loc>
<lastmod>' . date( 'Y-m-d', strtotime( 'last weekday' ) ) . '</lastmod>
</sitemap>';
		return $sitemap_custom_items;
	}
);

function SearchFilter( $query ) {
	if ( $query->is_search ) {
		$query->set( 'post_type', 'post' );
	}
	return $query;
}
add_filter( 'pre_get_posts', 'SearchFilter' );

<?php
/**
 * The template name: Products
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package IncentFit
 */

add_action( 'wp_footer', 'if_fpjs_init', 20 );
function if_fpjs_init() { ?>
	<script>
		document.body.classList.add('large-screen');
		new fullpage('#fullpage',{
			licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
			menu: '.menu',
			css3: true,
			fixedElements: '.navbar, .fixed-footer',
			scrollOverflow: true,
			paddingTop: '70px',
			navigation: true,
			navigationPosition: 'right',
		}); 
			  
	</script>
<?php
}

get_header();
?>

	<div id="primary" class="content-area row p-0">
		<main id="main" class="site-main col-12 p-0">

		<?php if ( have_rows( 'slides' ) ) : ?>
		
			<div id="fullpage">
			
			<?php
			while ( have_rows( 'slides' ) ) :
		the_row();
		?>
		
				<?php if ( get_row_layout() == 'product' ) : ?>
				<!-- PRODUCT START -->
				<div style="background-image: url('<?php the_sub_field( 'background_image' ); ?>');" class="section">
					<div class="container">
						<div class="row mx-3 
						<?php
						if ( get_row_index() % 1 == 0 ) :
?>
justify-content-start<?php endif; ?> <?php
if ( get_row_index() % 2 == 0 ) :
							?>
							justify-content-end<?php endif; ?> mb-5">
							<div class="col-auto mb-5 p-0">
								<!--<span class="badge badge-pill badge-rose tag mb-5" style="/* background: rgba(196, 196, 196, 0.62); */">--><?php // the_sub_field( 'badge_name' ); ?><!--</span>-->
							</div><!-- .col-auto -->
						</div><!-- .row -->

						<div class="row mx-3 
						<?php
						if ( get_row_index() % 1 == 0 ) :
?>
justify-content-start<?php endif; ?> <?php
if ( get_row_index() % 2 == 0 ) :
							?>
							justify-content-end<?php endif; ?>">
							<?php $link = get_sub_field( 'page_link' ); ?>

							<div class="col-12 col-md-8 bg-primary text-light p-3 rounded">
								<h3 class="m-0 mb-2"><?php the_sub_field( 'title' ); ?></h3>

								<?php the_sub_field( 'description' ); ?>
							</div><!-- .col -->
						</div><!-- .row -->

						<div class="row mx-3 
						<?php
						if ( get_row_index() % 1 == 0 ) :
?>
justify-content-start<?php endif; ?> <?php
if ( get_row_index() % 2 == 0 ) :
							?>
							justify-content-end<?php endif; ?> pt-3">
							<div class="col-12 col-md-8 d-flex 
							<?php
							if ( get_row_index() % 2 == 0 ) :
?>
justify-content-start 
<?php
else :
								?>
								justify-content-end<?php endif; ?> p-0">
								<a class="btn btn-secondary btn-round btn-lg" href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
							</div>
						</div><!-- .row -->
					</div><!-- .container -->
				</div><!-- section -->
				<!-- PRODUCT END -->
				<!-- HERO START -->
				<?php elseif ( get_row_layout() == 'hero' ) : ?>
				<div style="background-image: url('<?php the_sub_field( 'background_image' ); ?>');" class="section">
					<div class="container px-5">
						<h1 class="text-light"><?php the_sub_field( 'title' ); ?></h1>
					</div><!-- .container -->
				</div><!-- .section -->
				<!-- HERO END -->
				<!-- FOOTER START -->
				<?php elseif ( get_row_layout() == 'features_table' ) : ?>
					<?php $link = get_sub_field( 'compare_products_link' ); ?>
					<div style="background-image: url('<?php the_sub_field( 'background_image' ); ?>');" class="section">
						<div class="containe px-5">
							<div class="row justify-content-center">
							<div class="col-12 text-center">
							<h2 class="text-center text-white"><?php the_sub_field( 'heading' ); ?></h2>

								<a class="btn btn-secondary btn-round btn-lg text-white" href="<?php echo $link['url']; ?> <?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
							</div>
								
							</div><!-- .row -->
						</div><!-- .container -->
					</div><!-- .section -->
					<?php endif; ?>
					<!-- FOOTER END -->
				<?php endwhile; ?>
			<?php endif; ?>
		</main><!-- #main -->
	</div><!-- #primary --> 

	<div class="fixed-footer fixed-bottom">
		<div class="row align-items-center justify-content-center">
			<h4 class="mr-md-3 py-md-3 px-md-5 text-center"><?php the_field( 'footer_cta_text' ); ?></h4>

			<a class="btn text-white btn-primary btn-round" href="<?php the_field( 'footer_button_value' ); ?>" ><?php the_field( 'footer_button_text' ); ?></a>
		</div><!-- .row -->
	</div><!-- .fixed-footer -->

<?php
get_footer();



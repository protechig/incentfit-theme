<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package IncentFit
 */

?>

	</div><!-- #content -->
	<!-- CONTENT END -->
	<!-- FOOTER START -->
	<footer id="colophon" class="footer site-footer text-center footer-black">
		<div class="container-fluid">
		<div class="row justify-content-between align-items-center">
		<div class="footer-brand col-sm-12 col-md-3 col-lg-4 ml-md-2 ml-lg-3 text-center text-md-left">
			<?php the_custom_logo( 'footer-brand' ); ?>
		</div>
		<ul class="social-buttons float-lg-center text-center mx-auto mx-md-0">
				<li>
					<a href="https://twitter.com/incentfit" class="btn btn-just-icon btn-link btn-twitter">
					<i class="fab fa-twitter-square"></i>
					</a>
				</li>

				<li>
					<a href="https://www.facebook.com/incentfit" class="btn btn-just-icon btn-link btn-facebook">
					<i class="fab fa-facebook"></i>
					<div class="ripple-container"></div></a>
				</li>

				<li>
					<a href="https://www.linkedin.com/company/incentfit/" class="btn btn-just-icon btn-link btn-linkedin">
					<i class="fab fa-linkedin"></i>
					</a>
				</li>
			</ul>
			
			<div class="pull-center float-lg-right col-sm-12 col-md-auto col-lg-4 text-center text-md-right">
			<?php
			wp_nav_menu(
				 array(
					 'menu'           => 'Footer', // Do not fall back to first non-empty menu.
					 'theme_location' => '__no_such_location',
					 'fallback_cb'    => false, // Do not fall back to wp_page_menu()
				 )
				);
			?>
			</div>
		</div>

<div class="row">
<div class="small copyright pull-center col-md-12">
				Copyright © <?php echo date( 'Y' ); ?> IncentFit Corporation. All Rights Reserved.
			</div>
</div>
			
		</div>
	</footer><!-- #colophon -->
	<!-- FOOTER END -->
</div><!-- #page -->
<!-- PAGE END -->
<?php wp_footer(); ?>

</body>
</html>

var GymFinder = {
	map: null,
	request: null,
	locationFilter: "",
	nameFilter: "",
	categoryFilter: "",
	loc: {
		zip: "10013",
		lat: "40.720502",
		lng: "-74.001259"
	},
	markersArray: [],
	SetUpPage: function() {
		if (typeof locGeom !== 'undefined') {
			GymFinder.loc.zip = "";
			GymFinder.loc.lat = locGeom.location.lat;
			GymFinder.loc.lng = locGeom.location.lng;
		}
		else {
			navigator.geolocation.getCurrentPosition(function(position) {
				GymFinder.loc.zip = "";
				GymFinder.loc.lat = position.coords.latitude;
				GymFinder.loc.lng = position.coords.longitude;
			});
		}
	},
	InitializeMap: function () {
		var mapOptions = {
			center: new google.maps.LatLng(this.loc.lat, this.loc.lng),
			zoom: 15,
			minZoom: 11,
			maxZoom: 16,
			streetViewControl: false,
			scaleControl: true,
			zoomControlOptions: {
				position: google.maps.ControlPosition.LEFT_TOP
			},
			fullscreenControlOptions: {
				position: google.maps.ControlPosition.BOTTOM_LEFT
			}
		};
		this.map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		// If there is a bounds object passed in, make sure it's in view
		if (typeof locGeom !== 'undefined' && typeof locGeom.bounds !== 'undefined')
			this.map.fitBounds(new google.maps.LatLngBounds(new google.maps.LatLng(locGeom.bounds.southwest.lat, locGeom.bounds.southwest.lng), new google.maps.LatLng(locGeom.bounds.northeast.lat, locGeom.bounds.northeast.lng)));
		// Add a listener for when the map is dragged or zoomed
		google.maps.event.addListener(this.map, 'dragend', function () {
			GymFinder.LoadResults();
		});
		google.maps.event.addListener(this.map, 'zoom_changed', function () {
			GymFinder.LoadResults();
		});
		// View the search box and link it to the UI element.
		this.map.controls[google.maps.ControlPosition.TOP].push(document.getElementById('filter_location'));
		this.map.controls[google.maps.ControlPosition.TOP].push(document.getElementById('filter_name'));
		this.map.controls[google.maps.ControlPosition.TOP].push(document.getElementById('filter_category'));
		this.map.controls[google.maps.ControlPosition.RIGHT_TOP].push(document.getElementById('sidebox'));
		var searchBox = new google.maps.places.SearchBox(document.getElementById('location'));
		// Listen for the event fired when the user selects an item from the pick list. Retrieve the matching places for that item.
		google.maps.event.addListener(searchBox, 'places_changed', function() {
			var places = searchBox.getPlaces();
			GymFinder.loc.lat = places[0].geometry.location.lat();
			GymFinder.loc.lng = places[0].geometry.location.lng();
			GymFinder.ProcessLocation();
		});
		this.LoadResults();
	},
	ProcessLocation: function() {
		var latlng = new google.maps.LatLng(GymFinder.loc.lat, GymFinder.loc.lng);
		GymFinder.map.panTo(latlng);
		GymFinder.LoadResults();
	},
	LoadResults: function () {
		if(this.request && this.request != 4)
			this.request.abort();
		var bounds = this.map.getBounds();
		if (!bounds)
			return;
		var pos = this.map.getCenter();
		this.loc.lat = pos.lat();
		this.loc.lng = pos.lng();
		this.nameFilter = jQuery('#name').val();
		this.categoryFilter = jQuery('#category').val();
		var radius = (google.maps.geometry.spherical.computeDistanceBetween(pos, bounds.getNorthEast()) * 0.000621371).toFixed(2);
		this.request = jQuery.ajax({
			url: "https://webapp.incentfit.com/ajax/ironhide.php",
			type: "POST",
			data: { svc: 'SearchPublicGyms', lat: this.loc.lat, lng: this.loc.lng, rad: radius, cat: this.categoryFilter, searchTerm: this.nameFilter },
			//dataType: "json",
			success: function(data) {
				if (data.success) {
					GymFinder.ClearMarkers();
					data.gyms = data.payload;
					if (data.gyms.length > 0) {
						var side = '';
						for (var i = 0; i < data.gyms.length; i++) {
							var gym = data.gyms[i];
							if (gym.Latitude && gym.Longitude) {
								var marker = new google.maps.Marker({ 
									position: new google.maps.LatLng(gym.Latitude, gym.Longitude), 
									map: GymFinder.map, 
									draggable: false, 
									animation: null, 
									icon: (gym.OfferingDiscount ? 'https://webapp.incentfit.com/common/img/pin-red-solid-4.png' : 'https://webapp.incentfit.com/common/img/pin-blue-solid-4.png'),
									zIndex: (gym.OfferingDiscount ? 2 : 1),
									info: new google.maps.InfoWindow({
										content: '<b><a href="/products/corporate-gym-discounts/' + gym.GymFinderURLEnd + '">' + gym.Title + '</a></b><br/>' + gym.Address + '<br/>' + (gym.Neighborhood ? gym.Neighborhood + '<br/>' : '') + gym.City + ', ' + gym.State + ' ' + gym.ZipCode + (gym.OfferingDiscount ? '</br><i>Offers Corporate Discount</i>' : '')
									})
								});
								google.maps.event.addListener(marker, 'click', function (marker) { 
									return function () { 
										marker.info.open(GymFinder.map, marker);
									}
								} (marker));
								GymFinder.markersArray.push(marker);
							}
							else
								GymFinder.markersArray.push(null);
						}
					}
				}
				else {
					console.log(data.message);
				}
			},
			error: function(jqXHR, textStatus, errorMessage) {
				console.log(errorMessage);
			},
			complete: function(jqXHR, textStatus) {
				jQuery('.loading').hide();
			}
		});
	},
	ClearMarkers: function () {
		for (var i = 0; i < this.markersArray.length; i++) {
			if (this.markersArray[i])
				this.markersArray[i].setMap(null);
		}
		this.markersArray.length = 0;
	}
}
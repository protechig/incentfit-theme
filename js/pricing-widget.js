// VueJS For Pricing Calculator Widget
var app = new Vue( {
  el: '#app', // element to initalize on
  data: {
    pageType: parseInt( sliderType.calcType ),
    numEmployees: 200, // { int } # Employees updates from slider
    optionsSelected: [ 'corpRatesDeals' ], // Stores selected options
    addonsSelected: [], // Stores selected Add Ons
    addonPrices: {
      implementation: { upfront: 2000, annual: 0 },
      eligibility: { upfront: 0, annual: 0 },
      sso: { upfront: 1000, annual: 0 },
      whitelabeling: { upfront: 10000, annual: 300 } // TODO: ignored
    },
    incentFitFees: null, // Total fees paid to IncentFit
    rewardsPaid: null, // Total Rewards paid to employees
    options: [

      // Options to display
      {
        name: 'corpRatesDeals', // Name of the option
        prettyName: 'Corporate Rates & Deals',
        description: 'Best for saving employees money on gym memberships',
        employeePayoutMultiplier: 0
      },
      {
        name: 'challenges',
        prettyName: 'Wellness Challenges',
        description: 'Best for team building and engagement',
        employeePayoutMultiplier: 0
      },
      {
        name: 'rewards',
        prettyName: 'Activity Rewards',
        description: 'Best for encouraging exercise and healthy behavior',
        employeePayoutMultiplier: 8.2
      },
      {
        name: 'reimbursements',
        prettyName: 'Fitness Reimbursements',
        description: 'Best for paying employees back for fitness costs',
        employeePayoutMultiplier: 17
      }
    ],
    pricing: [
      {
        productMix: {
          corpRatesDeals: true,
          challenges: false,
          rewards: false,
          reimbursements: false
        },
        minPrice: 0,
        tiers: {
          0: 0,
          50: 0.25,
          100: 0.2,
          250: 0.15,
          500: 0.13,
          750: 0.12,
          1000: 0.1,
          5000: 0.07
        }
      },
      {
        productMix: {
          corpRatesDeals: true,
          challenges: true,
          rewards: false,
          reimbursements: false
        },
        minPrice: 75,
        tiers: {
          0: 1.2,
          100: 1.0,
          250: 0.9,
          500: 0.8,
          750: 0.7,
          1000: 0.6,
          5000: 0.5
        }
      },
      {
        productMix: {
          corpRatesDeals: true,
          challenges: false,
          rewards: true,
          reimbursements: false
        },
        minPrice: 100,
        tiers: {
          0: 1.7,
          100: 1.5,
          250: 1.3,
          500: 1.15,
          750: 1.0,
          1000: 0.85,
          5000: 0.75
        }
      },
      {
        productMix: {
          corpRatesDeals: true,
          challenges: false,
          rewards: false,
          reimbursements: true
        },
        minPrice: 125,
        tiers: {
          0: 1.9,
          100: 1.65,
          250: 1.45,
          500: 1.25,
          750: 1.1,
          1000: 0.95,
          5000: 0.82
        }
      },
      {
        productMix: {
          corpRatesDeals: true,
          challenges: true,
          rewards: true,
          reimbursements: false
        },
        minPrice: 150,
        tiers: {
          0: 2.25,
          100: 2.0,
          250: 1.8,
          500: 1.5,
          750: 1.35,
          1000: 1.2,
          5000: 1.0
        }
      },
      {

        //TODO: duplicate, should reference the other
        productMix: {
          corpRatesDeals: true,
          challenges: false,
          rewards: true,
          reimbursements: true
        },
        minPrice: 125,
        tiers: {
          0: 1.9,
          100: 1.65,
          250: 1.45,
          500: 1.25,
          750: 1.1,
          1000: 0.95,
          5000: 0.82
        }
      },
      {
        productMix: {
          corpRatesDeals: true,
          challenges: true,
          rewards: false,
          reimbursements: true
        },
        minPrice: 150,
        tiers: {
          0: 2.45,
          100: 2.15,
          250: 1.95,
          500: 1.6,
          750: 1.45,
          1000: 1.3,
          5000: 1.07
        }
      },
      {

        //TODO: duplicate, should reference the other
        productMix: {
          corpRatesDeals: true,
          challenges: true,
          rewards: true,
          reimbursements: true
        },
        minPrice: 150,
        tiers: {
          0: 2.45,
          100: 2.15,
          250: 1.95,
          500: 1.6,
          750: 1.45,
          1000: 1.3,
          5000: 1.07
        }
      },
      {
        productMix: {
          corpRatesDeals: false,
          challenges: false,
          rewards: false,
          reimbursements: false
        },
        minPrice: 0,
        tiers: { 0: 0, 100: 0, 250: 0, 500: 0, 750: 0, 1000: 0, 5000: 0 }
      },
      {

        //TODO: duplicate, should reference the other
        productMix: {
          corpRatesDeals: false,
          challenges: true,
          rewards: false,
          reimbursements: false
        },
        minPrice: 75,
        tiers: {
          0: 1.2,
          100: 1.0,
          250: 0.9,
          500: 0.8,
          750: 0.7,
          1000: 0.6,
          5000: 0.5
        }
      },
      {

        //TODO: duplicate, should reference the other
        productMix: {
          corpRatesDeals: false,
          challenges: false,
          rewards: true,
          reimbursements: false
        },
        minPrice: 100,
        tiers: {
          0: 1.7,
          100: 1.5,
          250: 1.3,
          500: 1.15,
          750: 1.0,
          1000: 0.85,
          5000: 0.75
        }
      },
      {

        //TODO: duplicate, should reference the other
        productMix: {
          corpRatesDeals: false,
          challenges: false,
          rewards: false,
          reimbursements: true
        },
        minPrice: 125,
        tiers: {
          0: 1.9,
          100: 1.65,
          250: 1.45,
          500: 1.25,
          750: 1.1,
          1000: 0.95,
          5000: 0.82
        }
      },
      {

        //TODO: duplicate, should reference the other
        productMix: {
          corpRatesDeals: false,
          challenges: true,
          rewards: true,
          reimbursements: false
        },
        minPrice: 150,
        tiers: {
          0: 2.25,
          100: 2.0,
          250: 1.8,
          500: 1.5,
          750: 1.35,
          1000: 1.2,
          5000: 1.0
        }
      },
      {

        //TODO: duplicate, should reference the other
        productMix: {
          corpRatesDeals: false,
          challenges: false,
          rewards: true,
          reimbursements: true
        },
        minPrice: 125,
        tiers: {
          0: 1.9,
          100: 1.65,
          250: 1.45,
          500: 1.25,
          750: 1.1,
          1000: 0.95,
          5000: 0.82
        }
      },
      {

        //TODO: duplicate, should reference the other
        productMix: {
          corpRatesDeals: false,
          challenges: true,
          rewards: false,
          reimbursements: true
        },
        minPrice: 150,
        tiers: {
          0: 2.45,
          100: 2.15,
          250: 1.95,
          500: 1.6,
          750: 1.45,
          1000: 1.3,
          5000: 1.07
        }
      },
      {

        //TODO: duplicate, should reference the other
        productMix: {
          corpRatesDeals: false,
          challenges: true,
          rewards: true,
          reimbursements: true
        },
        minPrice: 150,
        tiers: {
          0: 2.45,
          100: 2.15,
          250: 1.95,
          500: 1.6,
          750: 1.45,
          1000: 1.3,
          5000: 1.07
        }
      }
    ]
  },

  methods: {
    updateSlider() {
      console.log( this.numEmployees );
      pricingSlider.slider( 'option', 'value', this.numEmployees );
    },
    toggleAddOn: function( addonName ) {
      if ( this.addonsSelected.includes( addonName ) ) {
        var addOnIndex = this.addonsSelected.indexOf( addonName );
        if ( -1 !== addOnIndex ) {
          this.addonsSelected.splice( addOnIndex, 1 );
        }
      } else {
        this.addonsSelected.push( addonName );
      }
    },
    toggleOption: function( optionName, optionIndex ) {
      if ( this.optionsSelected.includes( optionName ) ) {
        var optionsIndex = this.optionsSelected.indexOf( optionName );
        if ( -1 !== optionIndex ) {
          this.optionsSelected.splice( optionsIndex, 1 );
        }
        this.$set( this.options[optionIndex], 'selected', false );
      } else {
        this.optionsSelected.push( optionName );
        this.$set( this.options[optionIndex], 'selected', true );
      }
    },

    getPricingFromProductMix: function( desiredProductMix ) {
      return this.pricing.filter( function( product ) {
        return (
          JSON.stringify( product.productMix ) ==
          JSON.stringify( desiredProductMix )
        );
      } )[0];
    },
    getPEPMFromPricing: function( pricing, numEmployees ) {
      var tiers = Object.keys( pricing.tiers )
        .filter( function( p ) {
          return p <= numEmployees;
        } )
        .map( function( item ) {
          return parseInt( item );
        } );
      var tier = Math.max.apply( null, tiers );
      return pricing.tiers[tier];
    },

    formatDollars: function( x ) {
      return (
        '$' +
        x
          .toFixed( 2 )
          .toString()
          .replace( /\B(?=(\d{3})+(?!\d))/g, ',' )
      );
    }
  },
  mounted: function() {
    if ( 99 != this.pageType ) {
      this.$set( this.options[this.pageType], 'selected', true );
    }
  },
  computed: {
    pricingOptions: function() {
      var that = this;
      var selectedPricing = this.selectedOptionPricing;
      return this.options.map( function( opt ) {
        potentialProductMix = JSON.parse(
          JSON.stringify( that.selectedProductMix )
        ); //doing this to clone the object so it's not being passed by reference
        if ( opt.selected ) {
          opt.displayPrice = that.getPEPMFromPricing(
            selectedPricing,
            that.numEmployees
          );
          opt.displayAdditional = false;
        } else {
          potentialProductMix[opt.name] = true;
          potentialPricing = that.getPricingFromProductMix( potentialProductMix );
          opt.displayPrice =
            that.getPEPMFromPricing( potentialPricing, that.numEmployees ) -
            that.getPEPMFromPricing( selectedPricing, that.numEmployees );
          opt.displayAdditional = Object.values( that.selectedProductMix ).some(
            function( p ) {
              return p;
            }
          );
        }
        return opt;
      } );
    },

    selectedOptions: function() {
      return this.options.filter( function( opt ) {
        return opt.selected;
      } );
    },

    selectedProductMix: function() {
      return this.options.reduce( function( acc, opt ) {
        acc[opt.name] = opt.selected ? true : false;
        return acc;
      }, {} );
    },

    selectedOptionPricing: function() {
      var desiredProductMix = this.selectedProductMix;
      return this.getPricingFromProductMix( desiredProductMix );
    },

    selectedAddons: function() {
      var that = this;
      return this.addonsSelected.map( function( name ) {
        return that.addonPrices[name] || { upfront: 0, annual: 0 };
      } );
    },

    incentFitFee: function() {
      var that = this;
      var pricing = this.selectedOptionPricing;
      return Math.max(
        pricing.minPrice,
        this.getPEPMFromPricing( pricing, this.numEmployees ) * this.numEmployees
      );
    },

    employeeFee: function() {
      var that = this;
      return this.selectedOptions.reduce( function( acc, opt ) {
        return opt.employeePayoutMultiplier * that.numEmployees;
      }, 0 );
    }
  }
} );

//var jquerySlider = document.getElementById( 'jquery-slider' );
//jquerySlider.slider().slider( 'pips' );
pricingSlider = jQuery( '#jquery-slider' );
pricingSlider
  .slider( {
    value: 200,
    min: 0,
    max: 10000,
    range: 'min'
  } )
  .slider( 'pips', {} );
pricingSlider.on( 'slidechange', function( event, ui ) {
  app._data.numEmployees = pricingSlider.slider( 'value' );
} );

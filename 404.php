<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package IncentFit
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<section class="page-header text-white error-404 not-found align-items-center" style="background: linear-gradient(
	  rgba(0, 0, 0, 0.65), 
	  rgba(0, 0, 0, 0.65)
	), url('<?php echo get_stylesheet_directory_uri() . '/assets/404.jpg'; ?>'); display: flex; justify-content: center; flex-direction: column">
				<header>
					<h1 class="page-title display-1"><?php esc_html_e( '404' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<h2 class="display-3"><?php esc_html_e( 'Page not found :(', 'incentfit' ); ?></h2>



				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>

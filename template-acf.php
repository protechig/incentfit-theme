<?php
/**
 * Template Name: Page with Content Blocks
 *
 * The template for displaying pages with ACF components.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incentfit
 */

get_header(); ?>

<div class="content-area">
		<main id="main" class="container-fluid">
			<?php
			// If the page is password protected...
			if ( post_password_required() ) :
				get_template_part( 'template-parts/content', 'password-protected' );
			else :
				incentfit_display_content_blocks();
			endif;
		?>
		<?php get_footer(); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package IncentFit
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
  <head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">


	<?php wp_head(); ?>
  </head>

<body <?php body_class( 'sidebar-collapse landing-page index-page' ); ?>>
  <div id="page" class="site">
	<nav class="navbar fixed-top navbar-expand-lg"  id="sectionsNav">
	  <div class="container justify-content-between">
		<div class="navbar-brand">
			<a href="/" class="custom-logo-link" rel="home" itemprop="url">
				<img src="<?php echo get_stylesheet_directory_uri() . '/assets/incentfit-logo.svg'; ?>">
			</a>
		</div>

		<button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="navbar-toggler-icon"></span>
		  <span class="navbar-toggler-icon"></span>
		  <span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse justify-content-end">
		<?php bootstrap_nav( 'ml-auto' ); ?>
		</div><!-- .collapse -->
	</div><!-- .container -->
  </nav><!-- .navbar -->

	<div id="content" class="site-content">

<?php
/**
 * Template Name: Find a Gym
 *
 * The template for displaynig the gym finder
 *
 * @package IncentFit
 */
class UnifyAPI {
	static protected $_UnifyAPIBaseURL = 'https://webapp.incentfit.com';
	static protected $_UnifyAPIURL     = 'https://webapp.incentfit.com/ajax/ironhide.php';

	public static function Request( $service, $params = null, $method = 'POST' ) {
		$dataArr = array( 'svc' => $service );
		if ( ! empty( $params ) ) {
			$dataArr = array_merge( $dataArr, (array) $params );
		}

		$options = array(
			'ssl'  => array(
				'verify_peer'      => false,
				'verify_peer_name' => false,
			),
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => $method,
				'content' => http_build_query( $dataArr ),
			),
		);
		$result  = file_get_contents( static::$_UnifyAPIURL, false, stream_context_create( $options ) );
		return json_decode( $result );
	}
}

// if it's a map view ($location is automatically set by WordPress in the gym_finder_rewrite_rule() function)
if ( $location == 'find-a-gym' ) {
	$location = null;
}
if ( ! empty( $location ) ) {
	$location  = htmlspecialchars_decode( str_replace( '-', ' ', $location ) );
	$geocoding = json_decode( file_get_contents( 'https://maps.googleapis.com/maps/api/geocode/json?key=' . GOOGLE_API_JS_KEY . '&address=' . urlencode( $location ) ) );
	if ( ! empty( $geocoding ) && ! empty( $geocoding->results ) ) {
		$geometry = $geocoding->results[0]->geometry;
	}
	if ( ! empty( $geometry ) ) {
		echo( "<script>var locGeom = JSON.parse('" . json_encode( $geometry ) . "');</script>" );
	}
}
// if it's a gym detail view ($gym is automatically set by WordPress in the gym_finder_rewrite_rule() function)
if ( ! empty( $gym ) ) {
	$gym    = htmlspecialchars_decode( str_replace( '-', ' ', $gym ) );
	$data   = array( 'gymFinderUrl' => $gym );
	$result = UnifyAPI::Request( 'GetPublicGym', $data );
	if ( ! empty( $result ) && $result->success && ! empty( $result->payload ) ) {
		$actWhtlst = $result->payload;
		$facCats   = array();
		foreach ( $actWhtlst->FacilityCategories as $facCat ) {
			$facCats[] = $facCat->Category;
		}
	} else {
		header( 'Location: /page-not-found' );
		die();
	}
}

// rename title and add meta description for gym finder sites
$title = 'Find a Fitness Facility Offering a Corporate Rate';
$meta  = 'Find local gyms and fitness facilities that accept IncentFit\'s discounts and corporate rates, to give you the best deal on company exercise programs.';
if ( ! empty( $location ) ) {
	$title = 'Corporate Gym Discounts in ' . $location;
	$meta  = $location . ' local gyms and fitness facilities that accept IncentFit\'s discounts and corporate rates. Get the best deal on company exercise programs.';
}
if ( ! empty( $gym ) ) {
	$title = $actWhtlst->Title . ' - ' . $actWhtlst->Address . ' ' . $actWhtlst->City . ', ' . $actWhtlst->State . ' ' . $actWhtlst->ZipCode . ' - Corporate Gym Discounts';
	$meta  = $gym . ' is a fitness facility in ' . $location . ' offering discounts and corporate rates. Get the best deal on company exercise and wellness programs.';
}
add_filter(
	 'wpseo_title',
	function() use ( $title ) {
		return $title;
	}
);
add_filter(
	 'wpseo_metadesc',
	function() use ( $meta ) {
		return $meta;
	}
);
add_filter(
	 'wpseo_canonical',
	function() {
		return 'https://' . $_SERVER['HTTP_HOST'] . strtok( $_SERVER['REQUEST_URI'], '?' );
	}
);

get_header();
?>

<?php if ( empty( $gym ) ) { ?>
<script type="text/javascript" async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo( GOOGLE_API_JS_KEY ); ?>&callback=GymFinder.InitializeMap&libraries=places,geometry"></script>
<section class="text-center">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h2 class="title pb-3"><?php echo $title; ?></h2>
			</div><!-- .col -->
		</div><!-- .row -->
		<div class="row justify-content-center">
			<div class="col-xl-3 col-lg-5 col-sm-6">
				<p><img height="30" src="https://webapp.incentfit.com/common/img/pin-red-solid-4.png"> Discounted Group Rate Available Now</p>
			</div><!-- .col -->
			<div class="col-xl-3 col-lg-5 col-sm-6">
				<p><img height="30" src="https://webapp.incentfit.com/common/img/pin-blue-solid-4.png"> Group Rate Pending Additional Demand</p>
			</div><!-- .col -->
		</div>
	</div><!-- .container-fluid -->
</section>
<section>
	<div id="filter_location" class="filter">
		<input type="text" placeholder="Location" maxlength="200" id="location" class="form-control" 
		<?php
		if ( ! empty( $location ) ) {
echo( 'value="' . str_replace( '-', ' ', $location ) . '"' );}
?>
/>
	</div>
	<div id="filter_name" class="filter d-none">
		<input type="text" placeholder="Name" maxlength="200" id="name" class="form-control" onchange="GymFinder.LoadResults();"/>
	</div>
	<div id="filter_category" class="filter">
		<select class="form-control" id="category" onchange="GymFinder.LoadResults();">
			<option value="0">All Facility Types</option>
			<?php
			// get list of categories
			$cats   = [];
			$result = UnifyAPI::Request( 'FacilityCategories' );
			if ( ! empty( $result ) && $result->success ) {
				$cats = $result->payload;
			}
			foreach ( $cats as $cat ) {
				echo( "<option value='" . $cat->CategoryID . "'>" . $cat->Category . '</option>' );
			}
			?>
		</select>
	</div>

	<?php if ( empty( $location ) ) { ?>
	<div id="sidebox" class="sidebox d-none d-md-block">
		<h3>Popular Locations</h3>
		<p><u><a href="/products/corporate-gym-discounts/New-York-NY/">New York, NY</a></u></p>
		<p><u><a href="/products/corporate-gym-discounts/San-Francisco-CA/">San Francisco, CA</a></u></p>
		<p><u><a href="/products/corporate-gym-discounts/Chicago-IL/">Chicago, IL</a></u></p>
		<p><u><a href="/products/corporate-gym-discounts/Brooklyn-NY/">Brooklyn, NY</a></u></p>
		<p><u><a href="/products/corporate-gym-discounts/Boston-MA/">Boston, MA</a></u></p>
		<p><u><a href="/products/corporate-gym-discounts/Los-Angeles-CA/">Los Angeles, CA</a></u></p>
		<p><u><a href="/products/corporate-gym-discounts/Houston-TX/">Houston, TX</a></u></p>
		<p><u><a href="/products/corporate-gym-discounts/Philadelphia-PA/">Philadelphia, PA</a></u></p>
		<p><u><a href="/products/corporate-gym-discounts/Washington-DC/">Washington, DC</a></u></p>
		<p><u><a href="/products/corporate-gym-discounts/Seattle-WA/">Seattle, WA</a></u></p>
		<p><u><a href="/products/corporate-gym-discounts/Houston-TX/">Houston, TX</a></u></p>
		<p><u><a href="/products/corporate-gym-discounts/Phoenix-AZ/">Phoenix, AZ</a></u></p>
		<p><u><a href="/products/corporate-gym-discounts/San-Jose-CA/">San Jose, CA</a></u></p>
	</div>
	<?php
	} else {
	$data = array(
		'lat'            => $geometry->location->lat,
	'lng'        => $geometry->location->lng,
	'rad'        => 5,
	'catID'      => 1,
	'searchTerm' => '',
	'limit'      => 25,
	);
$result   = UnifyAPI::Request( 'SearchPublicGyms', $data );
if ( ! empty( $result ) && $result->success ) {
		$gyms = $result->payload;
		echo( '<div id="sidebox" class="sidebox d-none d-md-block"><h3>Top Locations</h3>' );
		foreach ( $gyms as $gym ) {
			echo( '<p><a href="/products/corporate-gym-discounts/' . $gym->GymFinderURLEnd . '">' . $gym->Title . ' - ' . $gym->Address . '</a></p>' );
			}
		echo( '</div>' );
	}
	}
	?>
	<div id="map-canvas"></div>
</section>

<section class="p-md-5 text-center">
	<div class="container-fluid">
		<div class="row justify-content-center px-md-3">
			<div class="col-12 col-md-6 d-flex flex-column">
				<h3>Earn Rewards for Exercise</h3>
				<p>Visit ANY fitness facility and you can earn cash or other rewards each time you exercise. The <a href="/products/activity-rewards">IncentFit Rewards</a> wellness program makes corporate gym reimbursements easy!</p>
			</div><!-- .col -->
			<div class="col-12 col-md-6 d-flex flex-column">
				<h3>Corporate Discounts</h3>
				<p>The fitness facilities above can offer a corporate gym discount for you and your coworkers if your company signs up for IncentFit.</p>
			</div><!-- .col -->
		</div><!-- .row -->
		<div class="row justify-content-center px-md-3">
			<div class="col-12">
				<a href="/get-started" class="btn btn-lg btn-success">Sign Up Your Company</a>
			</div><!-- .col -->
		</div><!-- .row -->
	</div><!-- .container-fluid -->
</section>

<?php } else { ?>
<section class="p-md-5">
	<div class="container-fluid">
		<div class="row justify-content-center px-md-3">
			<div class="col-12">
				<h2>Corporate Gym Discounts</h2>
			</div><!-- .col -->
		</div><!-- .row -->
		<div class="row justify-content-center px-md-3">
			<div class="col-12 col-md-6 flex-column">
				<h3><?php echo( $actWhtlst->Title ); ?></h3>
				<?php
				if ( ! empty( $actWhtlst->GymAffiliateID ) && ! empty( $actWhtlst->LogoFileName ) ) {
					echo( '<img src="' . $actWhtlst->LogoFileName . '" class="img-fluid" alt="' . $actWhtlst->Title . ' gym logo" />' );
				}
				?>
				<h4>
					<?php echo( $actWhtlst->Address ); ?><br/>
					<?php
					if ( ! empty( $actWhtlst->Neighborhood ) ) {
echo( $actWhtlst->Neightborhood . '<br/>' );}
?>
					<?php echo( '<u><a href="/products/corporate-gym-discounts/' . str_replace( ' ', '-', $location ) . '/">' . $actWhtlst->City . ', ' . $actWhtlst->State . '</a></u> ' . $actWhtlst->ZipCode ); ?><br/>
					<?php echo( $actWhtlst->Country ); ?><br/>
					<?php
					if ( ! empty( $actWhtlst->Url ) ) {
echo( '<u><a href="' . $actWhtlst->Url . '" target="_new">View ' . $actWhtlst->Title . ' Website</a></u><br/>' );}
?>
					<?php
					if ( ! empty( $actWhtlst->Phone ) ) {
echo( $actWhtlst->Phone . '<br/>' );}
?>
				</h4>
				<br/>
				<p><?php echo( implode( ',', $facCats ) ); ?></p>
				<?php if ( ! $actWhtlst->Active ) { ?>
					<h2 style="color:red;">This location is permanently closed!</h2>
				<?php } elseif ( ( ! empty( $actWhtlst->GymAffiliateReferenceID ) && ! empty( $actWhtlst->AffiliateActive ) ) || isset( $actWhtlst->YelpDeal ) ) { ?>
					<h2 style="color:green;"><?php echo( $actWhtlst->Title ); ?> offers a discounted group rate<br/>to IncentFit companies!</h2>
				<?php } ?>
			</div><!-- .col -->
			<div class="col-12 col-md-6 d-flex flex-column">
				<img style="width:100%; margin-top:30px;" alt="<?php echo( $actWhtlst->Title ); ?>" src="https://maps.googleapis.com/maps/api/staticmap?key=<?php echo( GOOGLE_API_JS_KEY ); ?>&center=<?php echo( round( $actWhtlst->Latitude, 3 ) . ',' . round( $actWhtlst->Longitude, 3 ) ); ?>&zoom=16&scale=2&size=555x350&markers=icon:https://webapp.incentfit.com/common/img/pin-red-solid-4.png%7C<?php echo( round( $actWhtlst->Latitude, 3 ) . ',' . round( $actWhtlst->Longitude, 3 ) ); ?>">
			</div><!-- .col -->
			<?php if ( ! empty( $actWhtlst->GymDescription ) ) { ?>
			<div class="col-12">
				<h5><?php echo( $actWhtlst->GymDescription ); ?></h5>
			<?php } ?>
			</div><!-- .col -->
		</div><!-- .row -->
	</div><!-- .container-fluid -->
</section>

<section class="p-md-5 text-center">
	<div class="container-fluid">
		<div class="row justify-content-center px-md-3">
			<div class="col-12 col-md-6 d-flex flex-column">
				<h3>Corporate Discounts</h3>
				<?php if ( ! empty( $actWhtlst->GymAffiliateReferenceID ) ) { ?>
					<p><?php echo( $actWhtlst->Title ); ?> can offer you and your coworkers a corporate discount if your company signs up for IncentFit.</p>
				<?php } else { ?>
					<p>Fitness facilities like <?php echo( $actWhtlst->Title ); ?> can offer a corporate gym discount for you and your coworkers if your company signs up for IncentFit.</p>
				<?php } ?>
			</div><!-- .col -->
			<div class="col-12 col-md-6 d-flex flex-column">
				<h3>Earn Rewards for Exercise</h3>
				<p>You can earn cash or other rewards every time you exercise at ANY fitness facility. The <a href="/products/activity-rewards">IncentFit Rewards</a> wellness program makes gym reimbursements easy!</p>
			</div><!-- .col -->
		</div><!-- .row -->
		<div class="row justify-content-center px-md-3">
			<div class="col-12">
				<a href="/get-started" class="btn btn-lg btn-success">Sign Up Your Company</a>
			</div><!-- .col -->
		</div><!-- .row -->
	</div><!-- .container-fluid -->
</section>

<?php }
get_footer(); ?>

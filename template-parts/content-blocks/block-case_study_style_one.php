<?php
/**
 * The template used for displaying a Case Study Style One block.
 *
 * @package IncentFit
 */

// Set up fields.
$title           = get_sub_field( 'title' );
$button_url      = get_sub_field( 'button_url' );
$button_text     = get_sub_field( 'button_text' );
$button_color     = get_sub_field( 'button_color' );

$size = 'bg_img'; // (thumbnail, medium, large, full or custom size)
$bg_image = wp_get_attachment_image_url(get_sub_field('bg_image'), $size);

?>
<!-- CTA STYLE TWO SECTION START -->
<section class="row justify-content-center py-md-3 py-lg-5" style="background-image: url('<?php echo $bg_image; ?>');">
	<div class="col-12 col-md-10 col-lg-6 pb-0 p-4 p-md-0">
		<div class="container-fluid rounded p-2 p-md-3 p-lg-4" style="background-color: rgba(229, 229, 229, 0.76);">
			<div class="row justify-content-center text-center">
				<div class="col-12 p-0">
					<?php if ( $title ) : ?>
					<h3 class="title text-capitalize mt-0">"<?php echo esc_html( $title ); ?>"</h3>
					<?php endif; ?>
				</div><!-- col -->
			</div><!-- .row -->

			<div class="row justify-content-center pb-md-0">
				<div class="col-auto">
					<?php if ( $button_url ) : ?>
					<button type="button" class="btn btn-round <?php echo esc_html( $button_color ); ?>" onclick="location.href='<?php echo esc_url( $button_url ); ?>'"><?php echo esc_html( $button_text ); ?></button>
					<?php endif; ?>
				</div>
			</div>
		</div><!-- .container-fluid -->
	</div><!-- .col -->
</section><!-- .call-to-action .row -->
<!-- CTA STYLE TWO SECTION END -->
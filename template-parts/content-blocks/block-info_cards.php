<?php
/**
 * The template used for displaying a Info Cards block.
 *
 * @package IncentFit
 */

 // Set up fields.
$title      = get_sub_field( 'title' );
$info_cards = get_sub_field( 'info_cards' );
?>

<!-- INFO CARDS START -->
<section class="row justify-content-center bg-primary py-5">
	<div class="col-12 col-lg-8">
		<div class="container-fluid">
			<div class="row justify-content-center align-content-center mb-5">
				<div class="col-auto">
					<?php if ( $title ) : ?>
					<h2 class="title text-center text-white m-0">
						<?php echo esc_html( $title ); ?>
					</h2>
					<?php endif; ?>
				</div><!-- .col -->

				<div class="col-auto">
					<?php incentfit_button(); ?>
				</div><!-- .col -->
			</div><!-- .row -->
			
			<div class="row justify-content-center">
			<?php
			// Start repeater markup...
			if ( have_rows( 'info_cards' ) ) {

				while ( have_rows( 'info_cards' ) ) {
					// incrament row
					the_row();
					// variables
					$size  = 'info_cards'; // (thumbnail, medium, large, full or custom size)
					$image = wp_get_attachment_image_url( get_sub_field( 'image' ), $size );

					$title = get_sub_field( 'title' );
					$text  = get_sub_field( 'text' );
					$link  = get_sub_field( 'link' );
					?>
					<div class="col-12 col-sm-6 col-md-4">
						<div class="card card-blog card-plain mt-0 card-background" style="background-image: url('<?php echo $image; ?>'); opacity: 1;">
							<?php if ( $link ) : ?>
							<a href="<?php echo esc_html( $link ); ?>">
								<?php if ( $image ) : ?>
								<div class="card-body">
									
									<div class="card-title">
										<?php if ( $title ) : ?>
										<h3><?php echo esc_html( $title ); ?></h3>
										<?php endif; ?>

										<?php if ( $text ) : ?>
										<h4><?php echo esc_html( $text ); ?></h4>
										<?php endif; ?>
									</div><!-- .card-title -->

									<div class="colored-shadow" style="background-image: url('<?php echo $image; ?>'; opacity: 1;"></div>
								</div> <!-- .card-header -->
								<?php endif; ?>
							</a>
							<?php endif; ?>
						</div><!-- .card -->
					</div><!-- .col -->
					<?php
					}
				}
				?>
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div><!-- .col -->
</section><!-- .row -->
<!-- INFO CARDS END -->

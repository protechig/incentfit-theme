<?php
/**
 * The template used for displaying a Brand Logos block.
 *
 * @package IncentFit
 *
 */

 // Set up fields.
$brand_logos   = get_sub_field( 'brand_logos' );
$size = 'brand_logos';
?>

<!-- BRAND LOGOS START -->
<section class="row justify-content-center bg-white">
    <div class="col-12 col-lg-8 p-4">
        <div class="container-fluid">
            <div class="row justify-content-center align-content-center">

<?php
// Start repeater markup...
if( have_rows('brand_logos') ) {
	
	while( have_rows('brand_logos') ) {
        // incrament row
        the_row();
        // variables
        $logo = get_sub_field('logo');
        $link = get_sub_field('link');

        ?>
                        <div class="col-auto py-3 col-md-auto m-auto justify-content-center">
                            <?php if ( $logo ) : ?>
                            <a href="<?php echo esc_html( $link ); ?>">
                                <?php echo wp_get_attachment_image( $logo, $size ); ?>
                            </a>
                            <?php endif; ?>
                        </div><!-- .col -->
        <?php
    }
}
?>
                    </div><!-- .row -->
                </div><!-- .container-fluid -->
            </div><!-- .col -->
        </section><!-- .row -->
        <!-- BRAND LOGOS END -->

<?php
/**
 *  The template used for displaying fifty/fifty media/text.
 *
 * @package IncentFit
 */

// Set up fields.
$image_data      = get_sub_field( 'media_left' );
$size = 'fifty_fifty';
$text            = get_sub_field( 'text_primary' );

// Start a <container> with a possible media background.
?>
<!-- FFIFTY FIFTY MEDIA TEXT START -->
<section class="py-5 row bg-white justify-content-center px-sm-5">
	<div class="col-12 col-lg-8 justify-content-center">
		<div class="container-fluid">
			<div class="row justify-content-center mx-3 mx-md-0">
				<div class="col-12 col-md-6 text-center">
					<?php echo wp_get_attachment_image( $image_data, $size, "", array( "class" =>"img-fluid")  ); ?>
				</div><!-- .col -->

				<div class="col-12 col-md-6 rounded bg-secondary bg-light p-2 p-md-3 p-lg-3">
					<?php
						echo force_balance_tags( $text ); // WPCS XSS OK.
					?>
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div><!-- .col-->
</section><!-- section -->
<!-- FFIFTY FIFTY MEDIA TEXT END -->

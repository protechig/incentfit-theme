<?php
/**
 * The template used for displaying a Process block.
 *
 * @package IncentFit
 */

 // Set up fields.
$process = get_sub_field( 'process' );
$title   = get_sub_field( 'title' );
$arrow   = get_sub_field( 'arrow' );

$icon_one  = get_sub_field( 'icon_one' );
$title_one = get_sub_field( 'title_one' );
$text_one  = get_sub_field( 'text_one' );

$icon_two  = get_sub_field( 'icon_two' );
$title_two = get_sub_field( 'title_two' );
$text_two  = get_sub_field( 'text_two' );

$icon_three  = get_sub_field( 'icon_three' );
$title_three = get_sub_field( 'title_three' );
$text_three  = get_sub_field( 'text_three' );

$size = 'process';
?>

<!-- PROCESS START -->
<section class="p-md-5 text-center">
	<div class="col-12">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
				<?php if ( $title ) : ?>
					<h2 class="title text-center pb-3">
						<?php echo esc_html( $title ); ?>
					</h2>
				<?php endif; ?>
				</div><!-- .col -->
			</div><!-- .row -->
			
			<div class="row justify-content-center align-content-center">
				<div class="col-12 col-md-3 text-center">
				<?php if ( $icon_one ) : ?>
					<?php echo wp_get_attachment_image( $icon_one, 'process', '', array( 'class' => 'img-fluid' ) ); ?>
				<?php endif; ?>
				<?php if ( $title_one ) : ?>
					<h6><?php echo esc_html( $title_one ); ?></h6>
				<?php endif; ?>
				<?php if ( $text_one ) : ?>
					<p><?php echo esc_html( $text_one ); ?></p>
				<?php endif; ?>
				</div><!-- .col -->

				<div class="col-12 col-md-1 my-auto pb-4 d-none d-md-inline">
				<?php if ( $arrow ) : ?>
					<img src="<?php echo esc_html( $arrow ); ?>" class="img-fluid">
				<?php endif; ?>
				</div><!-- .col -->

				<div class="col-12 col-md-3 text-center">
				<?php if ( $icon_two ) : ?>
					<?php echo wp_get_attachment_image( $icon_two, 'process', '', array( 'class' => 'img-fluid' ) ); ?>
				<?php endif; ?>
				<?php if ( $title_two ) : ?>
					<h6><?php echo esc_html( $title_two ); ?></h6>
				<?php endif; ?>
				<?php if ( $text_two ) : ?>
					<p><?php echo esc_html( $text_two ); ?></p>
				<?php endif; ?>
				</div><!-- .col -->

				<div class="col-12 col-md-1 my-auto pb-4 d-none d-md-inline">
				<?php if ( $arrow ) : ?>
					<img src="<?php echo esc_html( $arrow ); ?>" class="img-fluid">
				<?php endif; ?>
				</div><!-- .col -->

				<div class="col-12 col-md-3 text-center">
				<?php if ( $icon_three ) : ?>
					<?php echo wp_get_attachment_image( $icon_three, 'process', '', array( 'class' => 'img-fluid' ) ); ?>
				<?php endif; ?>
				<?php if ( $title_three ) : ?>
					<h6><?php echo esc_html( $title_three ); ?></h6>
				<?php endif; ?>
				<?php if ( $text_three ) : ?>
					<p><?php echo esc_html( $text_three ); ?></p>
				<?php endif; ?>
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div><!-- .col -->
</section><!-- section -->
<!-- PROCESS END -->

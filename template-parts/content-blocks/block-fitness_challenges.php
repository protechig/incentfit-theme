<?php
/**
 * The template used for displaying a Fitness Challenges block.
 *
 * @package IncentFit
 *
 */

 // Enqueue Slick
 wp_enqueue_script('slick');

 add_action( 'wp_footer', 'if_slick_init', 20);

 function if_slick_init() {
     ?>
    <script>
        jQuery(document).ready(function( $ ) {
            $('#challenges-carousel').slick({
                responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
            });
        });
    </script>
     <?php
 }

 // Set up fields.
$title   = get_sub_field( 'title' );
$fitness_challenges   = get_sub_field( 'fitness_challenges' );
?>

<section class="row py-3 px-sm-4 p-lg-5">
    <div class="container-fluid">
<?php if ( $title ) : ?>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-8">
            <h2 class="text-center pb-3"><?php echo esc_html( $title ); ?></h2>
        </div><!-- .col -->
    </div><!-- .row -->

    <?php endif; ?>
        <div class="row projects-1 py-4" id="challenges-carousel" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>

            <?php
            // Start repeater markup...
            if( have_rows('fitness_challenges') ) {
                
                while( have_rows('fitness_challenges') ) {
                    // incrament row
                    the_row();
                    // variables
                    $category = get_sub_field('category');
                    $image = get_sub_field('image');
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    $button_text = get_sub_field('button_text');
                    $button_link = get_sub_field('button_link');
                    $button_color = get_sub_field('button_color');

                    $size = 'fitness_challenges';

                ?>
                    <?php if ( $image ) : ?>
                    <div class="col">
                        <div class="card card-raised card-background m-0 p-0" style="background-image: url('<?php echo $image; ?>'); background-repeat: no-repeat; background-position: center top; background-size: cover;">
                            <div class="card-body m-0 p-2 d-flex justify-content-end align-items-center flex-column pb-4">
                                <?php if ( $category ) : ?>
                                <h6 class="p-0 pt-3 m-0 card-category text-info"><?php echo esc_html( $category ); ?></h6>
                                <?php endif; ?>

                                <?php if ( $title ) : ?>
                                <h4 class="p-0 pt-3 m-0 card-title"><?php echo esc_html( $title ); ?></h4>
                                <?php endif; ?>

                                <?php if ( $description ) : ?>
                                    <p class="card-description"><?php echo esc_html( $description ); ?></p>
                                <?php endif; ?>

                                <?php if ( $button_text ) : ?>
                                <a href="<?php echo esc_html( $button_link ); ?>" class="btn btn-sm <?php echo esc_html( $button_color ); ?> btn-round">
                                    <?php echo esc_html( $button_text ); ?>
                                </a>
                                <?php endif; ?>
                            </div><!-- .card-body -->
                        </div><!-- .card -->
                    </div><!-- .col -->
                    <?php endif; ?>
                    <?php
                    }
                }
                ?>  
        </div><!-- .container-fluid -->
    </div><!-- .col -->
</section><!-- .row -->
<!-- FITNESS CHALLENGES END -->

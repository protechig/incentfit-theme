<?php
/**
 * The template used for displaying a Icon And Text block.
 *
 * @package IncentFit
 *
 */

 // Set up fields.
$icon_and_text   = get_sub_field( 'icon_and_text' );
$title           = get_sub_field( 'title' );
$item            = get_sub_field( 'itme' );
?>

<!-- ICON AND TEXT START -->
<section class="row justify-content-center p-5">
    <div class="col-12 col-lg-10">
        <div class="container-fluid p-lg-5">
            <div class="row justify-content-center">
                <div class="col-12">
                <?php if ( $title ) : ?>
                    <h2 class="title text-center m-0 pb-5">
                        <?php echo esc_html( $title ); ?>
                    </h2>
                <?php endif; ?>
                </div><!-- .col -->
            </div><!-- .row -->
            
            <div class="row justify-content-center">
            <?php
            // Start repeater markup...
            if( have_rows('item') ) {
                
                while( have_rows('item') ) {
                    // incrament row
                the_row();
                // variables
                $icon = get_sub_field('icon');
                $size = 'icon_and_text';
                $title = get_sub_field('title');
                $text = get_sub_field('text');
                $link = get_sub_field('link');

                ?>
                <div class="col-12 col-md-3 text-center">
                    <?php if ( $link ) : ?>
                    <a href="<?php echo esc_html( $link ); ?>">
                        <?php if ( $icon ) : ?>
                        <?php echo wp_get_attachment_image($icon, 'icon_and_text', "", array( "class" =>"img-fluid") ); ?>
                        <?php endif; ?>
                    </a>

                    <a href="<?php echo esc_html( $link ); ?>" class="text-dark">
                        <?php if ( $title ) : ?>
                        <h5><?php echo esc_html( $title ); ?></h5>
                        <?php endif; ?>

                        <?php if ( $text ) : ?>
                        <p><?php echo esc_html( $text ); ?></p>
                        <?php endif; ?>
                    </a>
                    <?php endif; ?>
                </div><!-- .col -->
                <?php
                }
            }
            ?>
            </div><!-- .icon-and-text .row -->
        </div><!-- .container-fluid -->
    </div><!-- .col -->
</section><!-- .row -->
<!-- ICON AND TEXT END -->

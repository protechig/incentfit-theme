<?php
/**
 * The template used for displaying a Team block.
 *
 * @package IncentFit
 */

 // Set up fields.
$title            = get_sub_field( 'title' );
$description      = get_sub_field( 'description' );
$background_image = get_sub_field( 'background_image' );
$team             = get_sub_field( 'team' );
?>

<!-- PRODUCT START -->
<section class="p-0 text-center bg-light pb-3 section section-basic row justify-content-center" team-4" style="background: linear-gradient(rgba(0,0,0,.8), rgba(0,0,0,.8)), url('<?php echo esc_html( $background_image ); ?>'); background-repeat: no-repeat; background-size: cover;">
	<div class="col-12 col-lg-8 p-0 px-3">
		<div class="container-fluid">  

		<?php if ( $title ) : ?>
		<div class="row">
			<div class="col-12">
				<h2 class="title text-white"><?php echo esc_html( $title ); ?></h2>
				<p class="description mb-5 text-white"><?php echo esc_html( $description ); ?></p>
			</div>
		</div><!-- .row -->
		<?php endif; ?>
		<?php
		// Start repeater markup...
		if ( have_rows( 'team' ) ) :
		?>

		<div class="row justify-content-center text-center pb-3">
			
		   <?php
		   while ( have_rows( 'team' ) ) :
the_row();
				// variables
				$picture      = get_sub_field( 'picture' );
				$name         = get_sub_field( 'name' );
				$title        = get_sub_field( 'title' );
				$description  = get_sub_field( 'description' );
				$social_icons = get_sub_field( 'social_icons' );

				$size = 'testimonials_bio';

				?>
			
				<div class="col-12 col-md-4">
					<div class="card card-profile">
						<?php if ( $picture ) : ?>
							<div class="card-header card-avatar">
								<?php echo wp_get_attachment_image( $picture, 'testimonials_bio', '', array( 'class' => 'img-fluid' ) ); ?>
							</div><!-- .card-header -->
					   <?php endif; ?>

						<div class="card-body">
							<h4 class="card-title p-0 pt-3 m-0"><?php echo esc_html( $name ); ?></h4>

							<?php if ( $title ) : ?>
								<h6 class="card-category text-muted"><?php echo esc_html( $title ); ?></h6>
							<?php endif; ?>

							<p class="card-description">
								<?php echo esc_html( $description ); ?>
							</p>
						</div>

						<div class="card-footer justify-content-center flex-wrap">
							<?php if ( have_rows( 'social_icons' ) ) : ?>
							
								<?php
								while ( have_rows( 'social_icons' ) ) :
the_row();
?>
									<a href="<?php the_sub_field( 'link' ); ?>" target=_blank class="btn btn-just-icon btn-round btn-<?php the_sub_field( 'icon' ); ?>">
										<i class="fab fa-<?php the_sub_field( 'icon' ); ?>"></i>

										<div class="ripple-container"></div>
									</a>
								<?php endwhile; ?>
							<?php endif; ?>    
						</div>
						
					</div><!-- .card -->
				</div><!-- .col -->
			

			<?php if ( get_row_index() % 3 == 0 ) : ?>
				</div><!-- .row -->
				<div class="row">
			<?php endif; ?>
			<?php endwhile; ?>
		<?php endif; ?>
		?>  
		</div><!-- .container-fluid -->
	</div><!-- .col -->
</section><!-- .row -->
<!-- PRODUCT END -->

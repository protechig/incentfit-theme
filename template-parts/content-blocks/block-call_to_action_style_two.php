<?php
/**
 * The template used for displaying a Call To Action Style Two block.
 *
 * @package IncentFit
 */

// Set up fields.
$title    = get_sub_field( 'title' );
$text     = get_sub_field( 'text' );
$size     = 'bg_img'; // (thumbnail, medium, large, full or custom size)
$bg_image = wp_get_attachment_image_url( get_sub_field( 'bg_image' ), $size );

?>
<!-- CTA STYLE TWO SECTION START -->
<section class="row justify-content-center py-md-3 py-lg-5" style="background-image: url('<?php echo $bg_image; ?>');">
	<div class="col-12 col-md-10 col-lg-6 pb-0 p-4 p-md-0">
		<div class="container-fluid">
			<div class="row justify-content-center rounded p-2 p-md-3 p-lg-4 bg-primary">
				<div class="col-12 p-3">
					<?php if ( $title ) : ?>
					<h2 class="title text-capitalize text-white mt-0"><?php echo esc_html( $title ); ?></h2>
					<?php endif; ?>

					<?php if ( $text ) : ?>
					<p class="text-white m-0"><?php echo esc_html( $text ); ?></p>
					<?php endif; ?>
				</div><!-- col -->
			</div><!-- .row -->

			<div class="row justify-content-center pb-md-0 mt-3">
				<div class="col-auto">
					<?php incentfit_button(); ?>
				</div>
			</div>
		</div><!-- .container-fluid -->
	</div><!-- .col -->
</section><!-- .call-to-action .row -->
<!-- CTA STYLE TWO SECTION END -->

<?php
/**
 *  The template used for displaying fifty/fifty text/text.
 *
 * @package IncentFit
 */

// Set up fields.
$text_primary    = get_sub_field( 'text_primary' );
$text_secondary  = get_sub_field( 'text_secondary' );
?>

<!-- FFIFTY FIFTY TEXT START -->
<section class="row justify-content-center py-5 bg-white px-sm-5">
	<div class="col-12 col-lg-8">
		<div class="container-fluid">
			<div class="row mx-3 mx-md-0">
				<div class="col col-12 col-md-6 rounded bg-secondary bg-light p-2 p-md-3 p-lg-3">
					<?php
						echo force_balance_tags( $text_primary ); // WPCS: XSS OK.
					?>
				</div><!-- .col -->
				
				<div class="col col-12 col-md-6 rounded bg-secondary bg-light p-2 p-md-3 p-lg-3">
					<?php
						echo force_balance_tags( $text_secondary ); // WPCS: XSS OK.
					?>
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div><!-- .col-->
</section><!-- section -->
<!-- FFIFTY FIFTY TEXT END -->

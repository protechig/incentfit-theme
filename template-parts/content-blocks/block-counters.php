<?php
/**
 * The template used for displaying a Counters block.
 *
 * @package IncentFit
 *
 */

 // Set up fields.
$counters   = get_sub_field( 'counters' );
?>

<!-- PRODUCT START -->
<section class="section section-basic row text-center bg-info text-white justify-content-center p-0">
    <div class="col-12 col-lg-8 p-0">
        <div class="container-fluid p-md-5">
            <div class="row justify-content-center my-5 my-md-0">
            <?php
            // Start repeater markup...
            if( have_rows('counters') ) {
                
                while( have_rows('counters') ) {
                    // incrament row
                    the_row();
                    // variables
                    $number = get_sub_field('number');
                    $label = get_sub_field('label');

                ?>
                <div class="col-12 col-md-auto">
                    <?php if ( $number ) : ?>
                        <h2 class="title text-white p-0 mt-0"><?php echo esc_html( $number ); ?></h2>
                    <?php endif; ?>

                    <?php if ( $label ) : ?>
                        <span class="text-white"><?php echo esc_html( $label ); ?></span>
                    <?php endif; ?>
                </div><!-- .col -->
                <?php
                }
            }
            ?>
            </div><!-- .row -->
        </div><!-- .container-fluid -->
    </div><!-- .col -->
</section><!-- .row -->
<!-- PRODUCT END -->
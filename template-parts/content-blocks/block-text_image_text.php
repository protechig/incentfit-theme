<?php
/**
 * The template used for displaying a Text Image Text block.
 *
 * @package IncentFit
 */

// Set up fields.
$title = get_sub_field( 'title' );
$text1 = get_sub_field( 'text1' );
$text2 = get_sub_field( 'text2' );
$image = get_sub_field( 'image' );

$size = 'text_img_text';

?>
<!-- CTA SECTION START -->
<section class="row justify-content-center py-5 bg-white">
	<div class="col-12 p-0">
		<div class="container-fluid">
			<div class="row">
			<?php if ( $title ) : ?>
				<div class="col-12 px-4 pb-4">
					<h2 class="title text-center m-0"><?php echo esc_html( $title ); ?></h2>
				</div><!-- col -->
				<?php endif; ?>
			</div>

			<div class="row justify-content-center px-md-3">
				<div class="col-12 col-md-4 d-flex flex-column">
					<?php if ( $text1 ) : ?>
						<?php the_sub_field( 'text1' ); ?>
					<?php endif; ?>
				</div><!-- col -->

				<div class="col-12 col-md-4 col-lg-auto d-flex flex-column justify-content-center">
					<?php if ( $image ) : ?>
						<?php echo wp_get_attachment_image( $image, 'text_img_text', '', array( 'class' => 'img-fluid mx-auto' ) ); ?>
					<?php endif; ?>
				</div><!-- col -->
				
				<div class="col-12 col-md-4 d-flex flex-column">
					<?php if ( $text2 ) : ?>
						<?php the_sub_field( 'text2' ); ?>
					<?php endif; ?>
				</div><!-- col -->
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div><!-- .col -->
</section><!-- .call-to-action .row -->
<!-- CTA SECTION END -->

<?php
/**
 * The template used for displaying a Text Slider Text block.
 *
 * @package IncentFit
 */

// Set up fields.
$title = get_sub_field( 'title' );
$text1 = get_sub_field( 'text1' );
$text2 = get_sub_field( 'text2' );

?>
<!-- CTA SECTION START -->
<section class="row justify-content-center py-5 bg-white">
	<div class="col-12 col-md-7 p-0">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 mb-3 d-flex justify-content-center">
					<?php if ( $title ) : ?>
					<h2 class="title text-center"><?php echo esc_html( $title ); ?></h2>

					<?php endif; ?>
				</div><!-- col -->
				<div class="col-12 col-md-4 d-flex flex-column">
					<?php if ( $text1 ) : ?>
						<?php the_sub_field( 'text1' ); ?>
					<?php endif; ?>
				</div><!-- col -->

				<div class="col-12 col-md-4 d-flex flex-column align-items-center justify-content-center">
							<?php $count = count( get_sub_field( 'slider' ) ); ?>
							<?php if ( have_rows( 'slider' ) ) : ?>
								<div class="carousel slide" id="carouselExampleIndicators" data-ride="carousel">
									<ol class="carousel-indicators">
										<?php for ( $i = 0; $i < ( $count ); $i++ ) : ?>
										<li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i; ?>" <?php echo ( $i == 0 ? 'class="active"' : '' ); ?>></li> 
										<?php endfor; ?>
								   </ol>
									<div class="carousel-inner">
										<?php
										while ( have_rows( 'slider' ) ) :
the_row();
?>
											<div class="carousel-item <?php echo ( get_row_index() == 1 ? 'active' : '' ); ?>">
												<img src="<?php the_sub_field( 'slide_image' ); ?>" />
											</div>

										<?php endwhile; ?>
									</div><!-- .carousel-inner -->
								</div><!-- .carousel-slide -->
							<?php endif; ?>
				</div><!-- col -->
				
				<div class="col-12 col-md-4 d-flex flex-column">
					<?php if ( $text2 ) : ?>
						<?php the_sub_field( 'text2' ); ?>
					<?php endif; ?>
				</div><!-- col -->
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div><!-- .col -->
</section><!-- .call-to-action .row -->
<!-- CTA SECTION END -->

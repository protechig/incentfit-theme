<?php
/**
 * The template used for displaying a generic content block.
 *
 * @package IncentFit
 */

// Set up fields.
$title           = get_sub_field( 'title' );
$content         = get_sub_field( 'content' );
$style         = get_sub_field( 'style' );
?>

<section class="row justify-content-center p-2 p-md-3 <?php echo esc_attr( $style ); ?>"> 
	<div class="col-12">
		<?php if ( $title ) : ?>
		<h2 class="generic-content-title"><?php echo esc_html( $title ); ?></h2>
		<?php endif; ?>

		<?php echo force_balance_tags( $content ); // WP XSS OK.?>
	</div>	
</section><!-- .generic-content -->
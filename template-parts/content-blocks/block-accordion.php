<?php
/**
 * The template used for displaying a Accordion block.
 *
 * @package IncentFit
 *
 */

 // Set up fields.
$title   = get_sub_field( 'title' );
$items   = get_sub_field( 'items' );
?>

<!-- FITNESS CHALLENGES START -->
<section class="p-0 text-center bg-light section section-basic row justify-content-center">
    <div class="col-12 col-lg-8 p-0 px-3">
        <div class="container-fluid p-md-5"> 
            <div class="projects-1">
            <?php if ( $title ) : ?>
        <h2 class="pb-3"><?php echo esc_html( $title ); ?></h2>
        <?php endif; ?>

            <div class="row justify-content-center">
            <div class="accordion col-12 text-left" id="accordionExample">
            <?php
            // Start repeater markup...
            if( have_rows('items') ) {
                
                while( have_rows('items') ) {
                    // incrament row
                    the_row();
                    // variables
                    $title = get_sub_field('title');
                    $content = get_sub_field('content');
                    $number = get_sub_field('number');
                ?>

                    
                        <div class="col-12 pb-3">
                            <div class="card-header bg-transparent" id="heading<?php echo esc_html( $number ); ?>">
                                <h5 class="mb-0">
                                    <?php if ( $title ) : ?>
                                    <a data-toggle="collapse" aria-expanded="false" data-target="#collapse<?php echo esc_html( $number ); ?>" aria-expanded="false" aria-controls="collapse<?php echo esc_html( $number ); ?>">
                                        <?php echo esc_html( $title ); ?>
                                        <i class="material-icons float-right text-info">keyboard_arrow_down</i>
                                    </a>
                                    <?php endif; ?>
                                </h5>
                            </div>

                            <div id="collapse<?php echo esc_html( $number ); ?>" class="collapse" aria-labelledby="heading<?php echo esc_html( $number ); ?>" data-parent="#accordionExample">
                                <?php if ( $content ) : ?>
                                <div class="card-body">
                                    <?php echo esc_html( $content ); ?>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    
                    <?php
                    }
                }
                ?>  
                </div>
                </div><!-- .row -->
            </div> 
        </div><!-- .container-fluid -->
    </div><!-- .col -->
</section><!-- .row -->
<!-- FITNESS CHALLENGES END -->
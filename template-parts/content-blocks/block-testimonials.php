<?php
/**
 * The template used for displaying a Testimonials block.
 *
 * @package IncentFit
 *
 */

// Set up fields.
$title = get_sub_field( 'title' );
$testimonials = get_sub_field( 'testimonials' );
?>

<div class="row justify-content-center section-dark p-md-3 align-items-center" style="min-height: 500px;">
    <div class="container-fluid testimonials testimonials-2 py-5">
        <h2 class="text-center text-light m-0 mb-4"><strong><?php the_sub_field('title'); ?></strong></h2>

        <div class="col-md-12 p-0">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner p-4 p-md-0">

<?php
// Start repeater markup...
if( have_rows('testimonials') ) {

  while( have_rows('testimonials') ) {
    // incrament row
    the_row();
    // variables
    $picture = get_sub_field( 'picture' );
    $name   = get_sub_field( 'name' );
    $text           = get_sub_field( 'text' );
    $job_title            = get_sub_field( 'job_title' );
    $stars            = get_sub_field( 'stars' );
    $size = 'testimonials_bio';

?>
            <div class="carousel-item <?php echo ( get_row_index() == 1 ? 'active' : '' ); ?>">
                <div class="card card-testimonial card-plain my-0">
                    <div class="card-avatar row justify-content-center">
                        <a href="#pablo">
                            <?php if ( $picture ) : ?>
                            <?php echo wp_get_attachment_image($picture, 'testimonials_bio', "", array( "class" =>"img-fluid")); ?>
                            <?php endif; ?>
                        </a><!-- .pablo -->
                    </div><!-- .card-avatar -->

                    <div class="footer col-12 text-center pt-4">
                        <?php switch ( $stars ) : 
                            case 1:
                            echo '<i class="material-icons text-success">star</i>';
                            break;
                            case 2:
                            echo '<i class="material-icons text-success">star</i>';
                            echo '<i class="material-icons text-success">star</i>';
                            break;
                            case 3:
                            echo '<i class="material-icons text-success">star</i>';
                            echo '<i class="material-icons text-success">star</i>';
                            echo '<i class="material-icons text-success">star</i>';
                            break;
                            case 4:
                            echo '<i class="material-icons text-success">star</i>';
                            echo '<i class="material-icons text-success">star</i>';
                            echo '<i class="material-icons text-success">star</i>';
                            echo '<i class="material-icons text-success">star</i>';
                            break;
                            case 5:
                            echo '<i class="material-icons text-success">star</i>';
                            echo '<i class="material-icons text-success">star</i>';
                            echo '<i class="material-icons text-success">star</i>';
                            echo '<i class="material-icons text-success">star</i>';
                            echo '<i class="material-icons text-success">star</i>';
                            break;
                        endswitch; ?>
                    </div><!-- .footer -->

                    <div class="card-body row justify-content-center align-items-center">
                        <?php if ( $text ) : ?>
                        <p class=" col-12 text-center card-description text-white"><?php echo esc_html( $text ); ?></p>
                        <?php endif; ?>

                        <?php if ( $name ) : ?>
                        <h4 class="card-title col-12 text-center"><?php echo esc_html( $name ); ?></h4>
                        <?php endif; ?>

                        <?php if ( $job_title ) : ?>
                        <h6 class="card-category text-muted col-12 text-center"><?php echo esc_html( $job_title ); ?></h6>
                        <?php endif; ?>
                    </div> <!-- .card-body -->
                </div><!-- .card-testimonial -->
            </div> <!-- .carousel-item -->
            <?php
            }
        }
        ?>

                </div><!-- .carousel .slide -->

                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div> <!-- .carousel-inner -->
        </div> <!-- .col-md-12 -->
    </div><!-- .container-fluid .testimonials .testimonials-2 .section-dark -->
</div><!-- .row -->
<!-- TESTIMONIALS END -->

<?php
/**
 * The template used for displaying a Call To Action Style Two block.
 *
 * @package IncentFit
 */

// Set up fields.
$title           = get_sub_field( 'title' );
$text            = get_sub_field( 'text' );
$button_url      = get_sub_field( 'button_url' );
$button_text     = get_sub_field( 'button_text' );

$size = 'info_cards'; // (thumbnail, medium, large, full or custom size)
$bg_image = wp_get_attachment_image_url(get_sub_field('bg_image'), $size);

?>
<!-- CTA STYLE TWO SECTION START -->
<section class="row" style="background-image: url('<?php echo $bg_image; ?>');">
	<div class="col-12 col-md-8">
		<div class="container-fluid">
			<div class="row content">
				<div class="col content">
					<?php if ( $title ) : ?>
					<h2 class="title"><?php echo esc_html( $title ); ?></h2>
					<?php endif; ?>

					<?php if ( $text ) : ?>
					<h4 class=""><?php echo esc_html( $text ); ?></h4>
					<?php endif; ?>
				</div><!-- col -->
			</div><!-- .row -->

			<div class="row justify-content-center">
				<div class="col-auto">
					<?php if ( $button_url ) : ?>
					<button type="button" class="btn" onclick="location.href='<?php echo esc_url( $button_url ); ?>'"><?php echo esc_html( $button_text ); ?></button>
					<?php endif; ?>
				</div>
			</div>
		</div><!-- .container-fluid -->
	</div><!-- .col -->
</section><!-- .call-to-action .row -->
<!-- CTA STYLE TWO SECTION END -->
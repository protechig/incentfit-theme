<?php
/**
 * The template used for displaying a hero with s typeriter affect.
 *
 * @package IncentFit
 */

 // enqueue Typewriter.js
// Set up fields.
$hero        = get_sub_field( 'hero_typewriter_slides' );
$slide_count = count( $hero );

// Start repeater markup...
if ( have_rows( 'hero_typewriter_slides' ) ) :

	// If there is more than one slide...
	// Loop through hero(s).
	while ( have_rows( 'hero_typewriter_slides' ) ) :
		the_row();

		// Set up fields.
		$title            = get_sub_field( 'title' );
		$tagline          = get_sub_field( 'tagline' );
		$background_image = get_sub_field( 'background_image' );
		$punctuation      = get_sub_field( 'punctuation' );

		// Start a <container> with possible block options.
		// incentfit_display_block_options( array(
		// 'container' => 'section', // Any HTML5 container: section, div, etc...
		// 'class'     => 'content-block hero', // Container class.
		// ) );
		// If we have a slider, set the animation in a data-attribute.
		if ( $slide_count > 0 ) : ?>
			<section class="row justify-content-center text-center" style="min-height: 600px; background: linear-gradient(rgba(0,139,171,.7), rgba(0,0,0,.0)), url(<?php echo esc_html( $background_image ); ?>); background-repeat: no-repeat; background-size: cover; background-position: <?php the_sub_field( 'background_image_position' ); ?>"><!-- HERO START -->
			<div class="col-12 col-lg-8 py-5">	
			<div class="container-fluid justify-content-center p-0 py-md-5">
					
					<div class="row justify-content-center">
						<div class="col-12 p-0 py-5">
							<div class="container py-5 px-2">
									
			<?php else : ?>
			<section class="row justify-content-center text-center"  style="min-height: 600px; background: linear-gradient(rgba(0,139,171,.7), rgba(0,0,0,.0)), url(<?php echo esc_html( $background_image ); ?>); background-repeat: no-repeat; background-size: cover;"><!-- HERO START -->
			<div class="col-12 col-lg-8 py-5">	
				<div class="container-fluid justify-content-center p-0 py-md-5">
					
					<div class="row justify-content-center">
						<div class="col-12 px-2 py-5">
							<?php endif; ?>

							<?php if ( $title ) : ?>
									<span class="hero-tagline-primary">
										<h2 class="title text-white">
											<strong><?php echo esc_html( $title ); ?></strong>
											<span id="typeApp"></span>
												<script type="text/javascript">
												var app = document.getElementById('typeApp');

												var typewriter = new Typewriter(app, {
													loop: true
												});

												<?php if ( have_rows( 'hero_typwriter' ) ) : ?>

													<?php
													while ( have_rows( 'hero_typwriter' ) ) :
													the_row();
													$text = get_sub_field( 'text' );
													?>

														<?php if ( get_row_index() == 1 ) { ?>

															typewriter.typeString('<?php the_sub_field( 'text' ); ?>')

														<?php } else { ?>
															.pauseFor(2500)
															.deleteAll()
															.typeString('<?php the_sub_field( 'text' ); ?>')
													<?php } ?>
													<?php endwhile; ?>
													<?php if ( $punctuation ) : ?>
													<?php echo esc_html( $punctuation ); ?>
													<?php endif; ?>
													start();
												<?php endif; ?>
												</script>
										</h2>
									</span>
								<?php endif; ?>

								<?php if ( $tagline ) : ?>
								<div class="row justify-content-center">
									<div class="col-12 col-md-8 p-2">
										<h4 class="display-4">
											<?php echo esc_html( $tagline ); ?>
										</h4>
									</div><!-- .col -->
								</div><!-- .row -->
								<?php endif; ?>

								<div class="row justify-content-center">
									<div class="col-12 col-md-8">
										<?php incentfit_button(); ?>
									</div><!-- .col -->
								</div><!-- .row -->

			<?php
				endwhile;
				if ( $slide_count > 0 ) :
		echo '
						</div><!-- .col -->
					</div><!-- .row -->
					</div>
				</div><!-- .hero -->
			</section><!-- .row --><!-- HERO END -->';
			endif;

		endif;
		?>

<?php
/**
 *  The template used for displaying fifty/fifty text/media.
 *
 * @package IncentFit
 */

// Set up fields.
$image_data      = get_sub_field( 'media_right' );
$size = 'fifty_fifty';
$text            = get_sub_field( 'text_primary' );
?>
<!-- FFIFTY FIFTY MEDIA TEXT START -->
<section class="row justify-content-center py-5 bg-white px-sm-5">
	<div class="col-12 col-lg-8">
		<div class="container-fluid">
			<div class="row mx-3 mx-md-0">
				<div class="col col-12 col-md-6 rounded bg-secondary bg-light p-2 p-md-3 p-lg-3">
					<?php
						echo force_balance_tags( $text ); // WPCS XSS OK.
					?>
				</div><!-- .col -->
				
				<div class="col col-12 col-md-6 text-center">
					<?php echo wp_get_attachment_image( $image_data, $size, "", array( "class" =>"img-fluid")  ); ?>
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div><!-- .col-->
</section><!-- section -->
<!-- FFIFTY FIFTY MEDIA TEXT END -->

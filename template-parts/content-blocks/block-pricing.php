<?php
/**
 * The template used for displaying the pricing calculator
 *
 * @package IncentFit
 */
wp_enqueue_script( 'vue' );
wp_enqueue_script( 'pricing-widget' );
wp_localize_script(
	'pricing-widget', 'sliderType', array(
		'calcType' => get_sub_field( 'pricing_calc_type' ),
	)
);

?>
<div id="app" class="row justify-content-center">
	<div class="col-12 col-lg-8 pricing p-3 text-center">
		<h2 class="text-center"><?php the_sub_field( 'title' ); ?></h2>
		<p class="mb-4"><?php the_sub_field( 'subtitle' ); ?></p>
		<div class="row justify-content-center align-items-center mb-4">
			<input type="number" v-model="numEmployees" v-on:keyup="updateSlider" class="py-1 text-center"></input>
			<label for="number" class="ml-2 mb-0">Employees</label>
		</div>
		<div v-if="pageType != 99" class="text-center">
			<h4 class="fee mb-4">{{ formatDollars(incentFitFee) }}</h4>
		</div>
		<div class="row mb-5 justify-content-center">
			<div class="col-10">
				<div id="jquery-slider"></div>
			</div>
		</div>
		<div v-if="pageType != 99" class="mb-4 text-center">
			<?php incentfit_button(); ?>
		</div>

		<div v-if="pageType == 99" class="row px-md-4">
			<div class="col-12 col-md-8">
				<div class="row justify-content-center">
					<div class="col-12">
						<h3>Choose Your Options</h3>
					</div>
				</div>

				<div class="row pricing-options">
					<div v-for="(option, index) in pricingOptions" class="option mb-3 mx-2 mx-md-auto mb-md-0" @click="toggleOption(option.name, index)" v-bind:class="{ selected: option.selected == true }">
						<h5 class="option-name">{{ option.prettyName }}</h5>
						
						<h6 class="option-description">{{ option.description }}</h6>

						<div class="option-price"><span v-if="option.displayAdditional">+</span> {{ formatDollars(option.displayPrice) }}<br></div>

						<div class="option-per">per employee per month</div>

						<div class="row justify-content-center mt-3">

						<div class="togglebutton">
							<label>
								<input type="checkbox" :checked="option.selected == true">
									<span class="toggle"></span>
							</label>
						</div><!-- .option -->
				</div><!-- .row -->
			</div><!-- .option -->
		</div><!-- .row.pricing-options -->

		<div class="add-ons">
			<div id="accordion" role="tablist" aria-multiselectable="true" class="card-collapse">
				<div class="card card-plain">
					<div class="card-header" role="tab" id="headingOne">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
							View Add Ons 

							<i class="material-icons">keyboard_arrow_down</i>
						</a>
					</div><!-- .card-header -->

					<div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
						<div class="card-body">
							<div v-bind:class="{ selected: addonsSelected.includes('implementation')}" class="row add-on mx-1" v-on:click="toggleAddOn('implementation')">
								<div class="togglebutton col-12 col-md-1 pb-3 text-center">
									<label>
										<input type="checkbox" :checked="addonsSelected.includes('implementation')">
										<span class="toggle"></span>
									</label>
								</div>
								<div class="col-8 pl-md-4">
									<p class="addon-name">Managed Implementation</p>
									<p>Most clients implement IncentFit within days using our free online implementation process. However, for larger companies and/or for groups with many customized needs, we offer a managed implementation process. We will setup weekly meetings with your team and manage the implementation project for you.</p>
								</div>
								<div class="addon-price col-3">
									<div class="price">$2k</div>
								</div>
							</div>
							<div v-bind:class="{ selected: addonsSelected.includes('eligibility')}" class="row add-on mx-1" v-on:click="toggleAddOn('eligibility')">
								<div class="togglebutton col-12 col-md-1 pb-3 text-center">
									<label>
										<input type="checkbox" :checked="addonsSelected.includes('eligibility')">
										<span class="toggle"></span>
									</label>
								</div>
								<div class="col-8 pl-md-4">
									<p class="addon-name">Eligibility File Sync</p>
									<p>We can stay in sync with your HRIS/payroll system to ensure we always have the most up-to-date employee list. Great for big companies and/or companies with high turn-over!</p>
								</div>
								<div class="addon-price col-3">
									<div class="price">FREE</div>
								</div>
							</div>
							<div class="row add-on mx-1" v-bind:class="{ selected: addonsSelected.includes('sso') }" v-on:click="toggleAddOn('sso')">
								<div class="togglebutton col-12 col-md-1 pb-3 text-center">
									<label>
										<input type="checkbox" :checked="addonsSelected.includes('sso')">
										<span class="toggle"></span>
									</label>
								</div>
								<div class="col-8 pl-md-4">
									<p class="addon-name">Single Sign On</p>
									<p>Let your employees sign into IncentFit using your company login system. Pricing varies depending on how your single sign on system works. Contact us for a free quote.</p>
								</div>
								<div class="addon-price col-3">
									<div class="price">$1k - $3k</div>
									<div class="subtext">depending on your system</div>
								</div>
							</div>
							<div class="row add-on mx-1" v-bind:class="{ selected: addonsSelected.includes('whitelabeling') }" v-on:click="toggleAddOn('whitelabeling')">
								<div class="togglebutton col-12 col-md-1 pb-3 text-center">
									<label>
										<input type="checkbox" :checked="addonsSelected.includes('whitelabeling')">
										<span class="toggle"></span>
									</label>
								</div>
								<div class="col-8 pl-md-4">
									<p class="addon-name">Whitelabeling</p>
									<p>In addition to removing the IncentFit brand, we can fully customize the look and feel of the app, website, and emails to match your desired design aesthetic. This can include a custom app, email styling, and web-portal styling. Great for companies with particular internal marketing standards!</p>
								</div>
								<div class="addon-price col-3">
									<div class="price">$5k - $20k</div>
									<div class="subtext">+$500 annual fee</div>
								</div>
							</div>


						</div><!-- .card-body -->
					</div><!-- #collapseOne -->	

				</div><!-- .card-plain -->
			</div><!-- #accordion -->
							</div><!-- .add-ons -->
			</div>	

			<div class="col-12 col-md-4">
				<div class="results">

					<h3>Pricing Calculator</h3>
						<div v-if="incentFitFee == 0">
							<div class="to-incentfit mb-4">
								<div class="fee-desc">Please input your employee count and select which products you're interested in.</div>
							</div>
							<a class="btn btn-secondary btn-round" href="/get-started">Talk to a Rep</a>
						</div>
						<div v-else-if="numEmployees > 9900">
							<div class="to-incentfit mb-4">
								<div class="fee-desc">Please get in touch with us for a custom quote.</div>
							</div>
							<a class="btn btn-secondary btn-round" href="/get-started">Talk to a Rep</a>
						</div>
						<div v-show="incentFitFee != 0">
							<div class="to-incentfit mb-4">
								<div class="fee-desc">Fee Paid To IncentFit</div>

								<div class="fee">{{ formatDollars(incentFitFee) }}</div>

								<div class="option-per">per month<br></div>
							</div><!-- .to-incentfit -->

							<div v-if="employeeFee > 0" class="to-employees mb-4">
								<div class="fee-desc">Estimated Rewards Budget for Employees</div>

								<div class="fee">{{ formatDollars(employeeFee) }}</div>

								<div class="option-per">per month<br></div>
							</div>
							<div class="calc-cta mb-3">
								<?php the_sub_field( 'form_embed' ); ?>
							</div>
						</div>
					</div> 
				</div>
			</div>
		</div><!-- .row -->
	</div><!-- .container.pricing -->
</div><!--#app -->

<?php
/**
 * The template used for displaying a CTA Style One block.
 *
 * @package IncentFit
 */

// Set up fields.
$title           = get_sub_field( 'title' );
$text            = get_sub_field( 'text' );
$style            = get_sub_field( 'style' );
$button_url      = get_sub_field( 'button_url' );
$button_text     = get_sub_field( 'button_text' );

?>
<!-- CTA STYLE ONE SECTION START -->
<section class="row bg-light p-2 p-md-5 text-center">
	<div class="col-12 col-md-8">
		<div class="container-fluid">
			<div class="row">
				<div class="col content">
					<?php if ( $title ) : ?>
					<h2 class="title"><?php echo esc_html( $title ); ?></h2>
					<?php endif; ?>

					<?php if ( $text ) : ?>
					<p class="display-4"><?php echo esc_html( $text ); ?></p>
					<?php endif; ?>
				</div><!-- col -->
			</div><!-- .row -->

			<div class="row justify-content-center">
				<div class="col-auto">
					<?php if ( $button_url ) : ?>
					<button type="button" class="btn btn-round btn-info" onclick="location.href='<?php echo esc_url( $button_url ); ?>'"><?php echo esc_html( $button_text ); ?></button>
					<?php endif; ?>
				</div>
			</div>
		</div><!-- .container-fluid -->
	</div><!-- .col -->
</section><!-- .row -->
<!-- CTA STYLE ONE SECTION END -->
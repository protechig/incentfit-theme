<?php
/**
 * The template used for displaying a generic content block.
 *
 * @package IncentFit
 */

// Set up fields.
$iframe_code = get_sub_field( 'iframe_code' );

// Start a <container> with possible block options.
// incentfit_display_block_options(
// );
?>

<section class="row justify-content-center m-0 p-5 <?php echo esc_attr( $animation_class ); ?> <?php echo esc_attr( $style ); ?>"> 

			<script>
			  function resizeIframe(obj) {
				 obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
			  }
			</script>
			<div class="col-12">
			<?php
			if ( $iframe_code ) :
			?>
				<iframe width="100%" height="auto" scrolling="no" onload="resizeIframe(this)" frameborder="0" src="<?php echo $iframe_code; ?>" />
		<?php endif; ?>

			</div>	
</section><!-- .generic-content -->

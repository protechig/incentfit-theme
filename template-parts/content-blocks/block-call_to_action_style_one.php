<?php
/**
 * The template used for displaying a Call To Action Style One block.
 *
 * @package IncentFit
 */

// Set up fields.
$title = get_sub_field( 'title' );
$text  = get_sub_field( 'text' );
$style = get_sub_field( 'style' );

?>
<!-- CTA STYLE ONE SECTION START -->
<section class="row bg-white p-2 p-md-5 text-center justify-content-center">
	<div class="col-12 col-md-10 col-lg-6 p-4 p-md-0">
		<div class="container-fluid p-0">
			<div class="row">
				<div class="col-12 p-0">
					<?php if ( $title ) : ?>
					<h2 class="title text-capitalize mt-0"><?php echo esc_html( $title ); ?></h2>
					<?php endif; ?>

					<?php if ( $text ) : ?>
					<p class=""><?php echo esc_html( $text ); ?></p>
					<?php endif; ?>
				</div><!-- col -->
			</div><!-- .row -->

			<div class="row justify-content-center">
				<div class="col-auto">
					<?php incentfit_button(); ?>
				</div>
			</div>
		</div><!-- .container-fluid -->
	</div><!-- .col -->
</section><!-- .row -->
<!-- CTA STYLE ONE SECTION END -->

<?php
/**
 * The template used for displaying a hero.
 *
 * @package IncentFit
 */

// Set up fields.
$title            = get_sub_field( 'title' );
$tagline          = get_sub_field( 'tagline' );
$background_image = get_sub_field( 'background_image' );
$punctuation      = get_sub_field( 'punctuation' );
?>
		<?php if ( $background_image ) : ?>


		<div class="row header-2 py-md-5" style="background: linear-gradient(rgba(0,139,171,.7), rgba(0,0,0,.0)), url('<?php echo esc_html( $background_image ); ?>'); background-repeat: no-repeat; background-size: cover; background-position: <?php the_sub_field( 'background_image_position' ); ?>"><!-- HERO START -->
				<div class="container">
					<?php if ( $title ) : ?>
						<div class="col-12 pt-5">
							<span class="hero-tagline-primary">
								<h2 class="title text-white">
									<strong><?php echo esc_html( $title ); ?></strong>
								</h2>
							</span>
						</div>
					<?php endif; ?>
					<div class="col-12 pb-5 row align-items-center">
						<div class="col-md-2 col-12">
							<?php incentfit_button(); ?>
						</div>
						<?php if ( $tagline ) : ?>
							<div class="col-12 col-md-10">
								<h3 class="my-4 font-weight-light text-white">
									<?php echo esc_html( $tagline ); ?>
								</h3>
							</div>
						<?php endif; ?>

					</div>
					</div>
					</div><!-- .header-2 --><!-- HERO END -->
<?php endif; ?>

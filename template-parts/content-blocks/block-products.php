<?php
/**
 * The template used for displaying a Product block.
 *
 * @package IncentFit
 *
 */

 // Set up fields.
$products   = get_sub_field( 'products' );
?>

<!-- PRODUCT START -->
<?php
// Start repeater markup...
if( have_rows('products') ) {
    
    while( have_rows('products') ) {
        // incrament row
        the_row();
        // variables
        $image = get_sub_field('image');
        $badge = get_sub_field('badge');
        $link = get_sub_field('link');
        $content = get_sub_field('content');
        $button_text = get_sub_field('button_text');
        $button_color = get_sub_field('button_color');

    ?>
<section class="p-5 row justify-content-center text-center"  style="background: linear-gradient(rgba(0,0,0,.3), rgba(0,0,0,.3)), url('<?php echo esc_html( $image ); ?>'); background-repeat: no-repeat; background-size: cover;">
    <div class="col-12 col-md-8 p-0">
        <div class="container-fluid">
            <!-- BADGE START -->
            <div class="row <?php if( get_row_index() % 1 == 0 ): ?>justify-content-start<?php endif; ?> <?php if( get_row_index() % 2 == 0 ): ?>justify-content-end<?php endif; ?> pb-5">
                <div class="col-auto pb-5 p-0">
                    <?php if ( $badge ) : ?>
                        <span class="badge badge-pill mb-5" style="background: rgba(196, 196, 196, 0.62);"><?php echo esc_html( $badge ); ?></span>
                    <?php endif; ?>
                </div><!-- .col -->
            </div><!-- .row -->
            <!-- BADGE END -->
            <!-- CONTENT START -->
            <div class="row <?php if( get_row_index() % 1 == 0 ): ?>justify-content-start<?php endif; ?> <?php if( get_row_index() % 2 == 0 ): ?>justify-content-end<?php endif; ?>">
                <div class="col-12 col-md-8 bg-primary text-left text-light p-md-3">
                    <?php if ( $link ) : ?>
                    <a href="<?php echo esc_html( $link ); ?>" class="text-light p-0 m-0 d-block">
                    <?php the_sub_field( 'content' ); ?>
                    </a>
                    <?php endif; ?>
                </div><!-- .col -->
            </div><!-- .row -->
            <!-- CONTENT END -->
            <?php if ( $button_text ) : ?>
            <!-- BUTTON START -->
            <div class="row <?php if( get_row_index() % 1 == 0 ): ?>justify-content-start<?php endif; ?> <?php if( get_row_index() % 2 == 0 ): ?>justify-content-end<?php endif; ?> p-0">
                <div class="col-12 col-md-8 p-0">
                    <div class="container p-0">
                        <div class="row <?php if( get_row_index() % 2 == 0 ): ?>justify-content-start <?php else: ?>justify-content-end<?php ?><?php endif; ?> ">
                            <div class="col-auto pt-2">
                                <a href="<?php echo esc_html( $link ); ?>" class="btn btn-round <?php echo esc_html( $button_color ); ?>"><?php echo esc_html( $button_text ); ?></a>
                            </div><!-- .col -->    
                        </div><!-- .row -->
                    </div><!-- .container -->
                </div><!-- .col -->
            </div><!-- .row -->
            <!-- BUTTON END -->
            <?php endif; ?>
        </div><!-- .container-fluid -->
    </div><!-- .col -->
</section><!-- .row -->
<!-- PRODUCT END -->

    <?php
    }
}
?>
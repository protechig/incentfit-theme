<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package IncentFit
 */
 // Set up fields.
 $color_class   = get_field('color_class');
 ?>

<article <?php post_class(' card ' . ' card-plain ' . ' card-blog ' . 'mt-0'); ?>>

<div class="container">
	<div class="row">
	<?php
		if (has_post_thumbnail()) {
	?>
	<div class="col-md-4 pb-sm-4 pb-md-0 pr-md-4 mb-4 mb-md-0">
		<figure class="featured-image index-image card-header card-header-image">
			<a href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark">
				<?php
					the_post_thumbnail('blog_grid' . ' img-fluid');
				?>
			</a>
		</figure><!-- .featured-image full-bleed -->
	</div>

	<?php 
		} 
	?>

	<div class="col-md-8 pl-md-3">
		<header class="entry-header">
			<h6 class="card-category <?php the_field('color_class') ?> m-0"><strong><?php echo get_the_category()[0]->name; ?></strong></h6>
			<?php

			if ( is_single() ) :
				the_title( '<h3 class="entry-title card-title">', '</h3>' );
			else :
				the_title( '<h3 class="entry-title card-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
			endif;
			if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
			</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->
  
		<div class="entry-content card-description">
			<?php
				the_excerpt($post->post_content, get_the_excerpt());
			?>
		</div><!-- .entry-content -->

		<footer class="entry-footer p-0">
			<!-- <p class="author">
				by, <strong><?php // echo get_the_author_meta('user_nicename', $post->post_author); ?></strong>
			</p> -->
		</footer><!-- .entry-footer -->
	</div>
	</div>
</div>

</article><!-- #post-## -->

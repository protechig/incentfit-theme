<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package IncentFit
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'incentfit' ); ?></h2>
</section>

<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package IncentFit
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
	<header class="page-header entry-header header-filter" data-parallax="true" style="background: url('<?php echo $image['0']; ?>') no-repeat center center; background-size: cover;">
		<div class="container">
			<div class="row">
				<div class="col col-md-8 ml-auto mr-auto text-center">
					<?php if ( 'post' == get_post_type() ) : ?>
					<h6 class="card-category <?php the_field( 'color_class' ); ?> m-0"><strong><?php echo get_the_category()[0]->name; ?></strong></h6>
					<?php endif; ?>
					
					<?php the_title( '<h1 class="entry-title title">', '</h1>' ); ?>
									
					<?php if ( 'post' == get_post_type() ) : ?>
						<?php the_tags( '<div class=""><span class="badge badge-pill text-white badge-primary mx-1">', '</span><span class="badge text-white badge-pill mx-1 badge-primary">', '</span></div>' ); ?>
						 <h4 class="card-title text-light">Written by <?php the_author(); ?></h4>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</header>
	<div class="main mb-5">
		<div class="container-fluid py-0 py-md-5" >
			<div class="row">
				<div class="col-md-6 ml-auto mr-auto entry-content px-4">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</article>

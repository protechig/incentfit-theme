<?php
/**
 * Customizer sections.
 *
 * @package IncentFit
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function incentfit_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'incentfit_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'incentfit' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'incentfit_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'incentfit' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'incentfit' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'incentfit_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'incentfit' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'incentfit_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'incentfit' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'incentfit_customize_sections' );

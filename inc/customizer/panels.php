<?php
/**
 * Customizer panels.
 *
 * @package IncentFit
 */

/**
 * Add a custom panels to attach sections too.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function incentfit_customize_panels( $wp_customize ) {

	// Register a new panel.
	$wp_customize->add_panel(
		'site-options', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => esc_html__( 'Site Options', 'incentfit' ),
			'description'    => esc_html__( 'Other theme options.', 'incentfit' ),
		)
	);
}
add_action( 'customize_register', 'incentfit_customize_panels' );
